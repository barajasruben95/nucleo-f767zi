/**
 * @file    lib_Delay.h
 * @date    6-September-2023
 * @brief   DELAY library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2023

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software,
    and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */

#ifndef LIB_DELAY_H_
#define LIB_DELAY_H_

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/**********************/
/***** LIBRARIES ******/
/**********************/
#include "stm32f7xx.h"

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/

/**
 * @brief    TIM6 initialization
 *
 * TIM6 is configured as follows:
 * - Update interrupt disabled
 * - Prescaler = (SystemCoreClock / 2) / 2000000) - 1
 * - Auto reloaded register = 1
 * - TIM6 tick at 0.5us or 2MHz
 * @param    None
 * @retval   None
 * @note     Configure SystemCoreClock to work with 216000000 Hz
 * @note     Library designed to work with SystemCoreClock = 216000000 Hz.
 * TODO Make library to work with different SystemCoreClock
 */
void lib_Delay_Init(void);

/**
 * @brief    Delay in microseconds using TIM6 polling mode
 * @param    micros Desired delay in microseconds
 * @retval   None
 * @note     Previous requeriment: initialize TIM6 with @lib_DELAY_Init
 * @note     Blocking code
 */
void lib_Delay_US(uint32_t micros);

/**
 * @brief    Delay in miliseconds using TIM6 polling mode
 * @param    millis Desired delay in miliseconds
 * @retval   None
 * @note     Previous requeriment: initialize TIM6 with @lib_DELAY_Init
 * @note     Blocking code
 */
void lib_Delay_MS(uint32_t millis);

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif
