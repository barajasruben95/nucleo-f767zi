/**
 * @file    lib_Delay.c
 * @date    6-September-2023
 * @brief   DELAY library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 */


/**********************/
/***** LIBRARIES ******/
/**********************/
#include "lib_Delay.h"

/*****************************/
/***** GLOBAL VARIABLES ******/
/*****************************/
/** Allow only one initialization */
static uint8_t lib_Delay_initialization = 0;

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/

void lib_Delay_Init(void)
{
	if(lib_Delay_initialization == 0)
	{
		/* Enable TIM6 clock */
		__HAL_RCC_TIM6_CLK_ENABLE();

		//Bit 0: CEN: Counter disabled
		//Bit 1 UDIS: The Update (UEV) event is generated by one of the following events: Counter overflow/underflow, Setting the UG bit or Update generation through the slave mode controller
		//Bit 2 URS: Any of the following events generates an update interrupt or DMA request if enabled. These events can be: Counter overflow/underflow, Setting the UG bit, Update generation through the slave mode controller
		//Bit 3 OPM: Counter is not stopped at update event
		//Bit 7 ARPE: TIM6_ARR register is not buffered
		//Bit 11 UIFREMAP: No re-mapping. UIF status bit is not copied to TIMx_CNT register bit 31
		TIM6->CR1 = 0x00;

		//Bits 6:4 MMS: Reset - the UG bit from the TIMx_EGR register is used as a trigger output (TRGO). If
		//reset is generated by the trigger input (slave mode controller configured in reset mode) then
		//the signal on TRGO is delayed compared to the actual reset
		TIM6->CR2 = 0x00;

		//Bit 0 UIE: Update interrupt disabled
		//Bit 8 UDE: Update DMA request disabled
		TIM6->DIER = 0x00;

		//Bits 15:0 PSC[15:0]: Prescaler value
		//SystemCoreClock = 216000000
		//PCLK1 = SystemCoreClock / 4 = 54000000 - for peripheral clocks
		//APBH1 = SystemCoreClock / 2 = 108000000 - for timer clocks
		//Preescaler = (APBH1 / frequency) - 1, where desired frequency = 2MHz
		//Each TIM6 tick is at 0.5us or 2MHz
		TIM6->PSC = (uint32_t)((SystemCoreClock / 2) / 2000000) - 1;

		//Bits 15:0 ARR[15:0]: auto reloaded register
		//Count 1 us per cycle (when TIM6->ARR = 0 and when TIM6->ARR = 1; after this, overflow in the SR register)
		TIM6->ARR = 1;

		/* lib_Delay initialization done */
		lib_Delay_initialization = 1;
	}
}

void lib_Delay_US(uint32_t micros)
{
	uint32_t tim6_cr1_tmp;
	uint32_t tim6_sr_tmp;

	/* Validate input, microseconds cannot be less than 1 */
	if(micros < 1)
	{
		return;
	}

	//Enable counter
	tim6_cr1_tmp = TIM6->CR1;
	tim6_cr1_tmp |= 0x01;
	TIM6->CR1 = tim6_cr1_tmp;

	/* Each TIM6 cycle is about 1us, loop to reach the desired time */
	while(micros)
	{
		do
		{
			/* Read status register */
			tim6_sr_tmp = TIM6->SR;

			/* Bit 0 UIF: Update interrupt flag. Update interrupt pending. This bit is set by hardware when the registers are updated */
			tim6_sr_tmp &= 0x1;
		}
		while( tim6_sr_tmp == 0);

		/* Clean flag */
		tim6_sr_tmp = TIM6->SR;
		TIM6->SR = 0xFFFFFFFE;

		/* Decrease delay variable */
		micros--;
	}

	/* Disable counter */
	tim6_cr1_tmp = TIM6->CR1;
	tim6_cr1_tmp &= 0xFFFFFFFE;
	TIM6->CR1 = tim6_cr1_tmp;

	/* Next cycle start from zero, clearing the count */
	TIM6->CNT = 0;
}

void lib_Delay_MS(uint32_t millis)
{
	lib_Delay_US(1000 * millis);
}
