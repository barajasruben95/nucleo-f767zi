/**
 ******************************************************************************
 * @file    lib_USART3.h
 * @author  Ruben Barajas
 * @version V1.0
 * @date    14-February-2024
 * @brief   HEADER - USART3 library for NUCLEO - F767ZI
 ******************************************************************************
 */

#ifndef lib_USART3_H_
#define lib_USART3_H_

/**********************/
/***** LIBRARIES ******/
/**********************/
#include "stm32f7xx.h"

/*****************************/
/***** DATA DEFINITIONS ******/
/*****************************/

///Enumeration to determine the base number to be used
typedef enum
{
	lib_USART3_binary = 2,       /*!< Binary numbers */
	lib_USART3_octal = 8,        /*!< Octal numbers */
	lib_USART3_decimal = 10,     /*!< Decimal numbers */
	lib_USART3_hexadecimal = 16  /*!< Hexadecimal numbers */
} lib_USART3_Base;

/**********************/
/***** FUNCTIONS ******/
/**********************/

/**
 * @brief    USART3 initialization (UART + GPIO)
 *
 * USART3 is configured as follows:
 * - Word Length = 8 Bits (8 data bit + no parity bit)
 * - Stop Bit    = One Stop bit
 * - Parity      = No parity
 * - BaudRate    = 115200
 * - Hardware flow control disabled (RTS and CTS signals)
 * @param    None
 * @retval   None
 * @note     No previous requirements
 *
 *  Instance | Function | Pin  | AF mode |
 * --------- |--------- |----- |-------- |
 *  USART3   | TX       | PD8  | AF7     |
 *  ^        | RX       | PD9  | AF7     |
 */
void lib_USART3_Init(void);

/**
 * @brief    Single or multiple bytes write
 * @param    data Data to be written through USART3 TX
 * @param    size Amount of data to be sent
 * @retval   None
 * @note     Previous requirement: initialize USART3, refer to @ref lib_USART3_Init
 */
void lib_USART3_Write(uint8_t *data, uint16_t size);

/**
 * @brief    Write number character by character
 * @param    number Number to write
 * @param    base Desired base to send the number (Ej. 14 can be: E hexadecimal, 14 decimal, 16 octal or 1110 binary)
 * @retval   None
 * @note     Previous requirement: initialize USART3, refer to @ref lib_USART3_Init
 */
void lib_USART3_WriteNumber(uint32_t number, lib_USART3_Base base);

/**
 * @brief    Single float number write
 * @param    number Float number to be written through USART3 TX
 * @param    decimalDigits Amount of digits after dot to send
 * @retval   None
 * @note     Previous requirement: initialize USART3, refer to @ref lib_USART3_Init
 */
void lib_USART3_WriteFloatingNumber(float number, uint8_t decimalDigits);

/**
 * @brief    Single byte read
 * @param    None
 * @retval   Data read from USART3 RX
 * @note     Previous requirement: initialize USART3, refer to @ref lib_USART3_Init
 */
uint8_t lib_USART3_Read(void);

#endif /* lib_USART3_H_ */
