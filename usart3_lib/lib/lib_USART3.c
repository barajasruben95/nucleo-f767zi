/**
 ******************************************************************************
 * @file    lib_USART3.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    14-February-2024
 * @brief   USART3 library for NUCLEO - F767ZI
 ******************************************************************************
 */

/**********************/
/***** LIBRARIES ******/
/**********************/
#include "lib_USART3.h"

/*****************************/
/***** GLOBAL VARIABLES ******/
/*****************************/
UART_HandleTypeDef lib_USART3_InitStruct;

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/
extern void lib_USART3_Error_Handler(void);

void lib_USART3_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructTx;
	GPIO_InitTypeDef GPIO_InitStructRx;

	/* Enable clocks first */
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_USART3_CLK_ENABLE();

	/* Configure TX GPIO */
	GPIO_InitStructTx.Pin = GPIO_PIN_8;
	GPIO_InitStructTx.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructTx.Pull = GPIO_NOPULL;
	GPIO_InitStructTx.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructTx.Alternate = GPIO_AF7_USART3;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStructTx);

	/* Configure RX GPIO */
	GPIO_InitStructRx.Pin = GPIO_PIN_9;
	GPIO_InitStructRx.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructRx.Pull = GPIO_NOPULL;
	GPIO_InitStructRx.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructRx.Alternate = GPIO_AF7_USART3;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStructRx);

	/* UART configuration */
	lib_USART3_InitStruct.Instance        = USART3;
	lib_USART3_InitStruct.Init.BaudRate   = 115200;
	lib_USART3_InitStruct.Init.WordLength = UART_WORDLENGTH_8B;
	lib_USART3_InitStruct.Init.StopBits   = UART_STOPBITS_1;
	lib_USART3_InitStruct.Init.Parity     = UART_PARITY_NONE;
	lib_USART3_InitStruct.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	lib_USART3_InitStruct.Init.Mode       = UART_MODE_TX_RX;
	lib_USART3_InitStruct.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&lib_USART3_InitStruct) != HAL_OK)
	{
		/* Initialization Error */
		lib_USART3_Error_Handler();
	}
}

void lib_USART3_Write(uint8_t *data, uint16_t size)
{
	/* Transmit data in polling mode */
	if(HAL_UART_Transmit(&lib_USART3_InitStruct, data, size, (uint32_t)100) != HAL_OK)
	{
		/* Handle error */
		lib_USART3_Error_Handler();
	}
}

void lib_USART3_WriteNumber(uint32_t number, lib_USART3_Base base)
{
	uint8_t localNumber = 0;
	int8_t i = 0;

	/* Set base format */
	if(base == lib_USART3_hexadecimal)
	{
		localNumber = 8;
	}
	else if(base == lib_USART3_decimal)
	{
		localNumber = 10;
	}
	else if(base == lib_USART3_octal)
	{
		localNumber = 11;
	}
	else if(base == lib_USART3_binary)
	{
		localNumber = 32;
	}
	else
	{
		/* Hexadecimal is by default */
		localNumber = 8;
	}

	/* Digits to be send */
	uint8_t digits[localNumber]; /* Array with max dimension to hold the number */
	while(number)
	{
		localNumber = number % base;

		/* Hexadecimal case */
		if(localNumber > 9)
		{
			if(localNumber == 10)
				digits[i]= 'A';
			else if(localNumber == 11)
				digits[i]= 'B';
			else if(localNumber == 12)
				digits[i]= 'C';
			else if(localNumber == 13)
				digits[i]= 'D';
			else if(localNumber == 14)
				digits[i]= 'E';
			else if(localNumber == 15)
				digits[i]= 'F';
			else
				digits[i]= '@'; /* Send at if fatal error is detected */
		}
		else
			digits[i]= localNumber + '0';

		number = (int32_t)(number / base);
		i++;
	}

	/* Send number through USART3 TX in polling mode */
	for(i--; i > -1; i--)
	{
		lib_USART3_Write(&digits[i], (uint16_t) 1);
	}
}

void lib_USART3_WriteFloatingNumber(float number, uint8_t decimalDigits)
{
	uint8_t toPrint;

	/* In case of negative numbers, print minus symbol */
	if (number < 0.0)
	{
		toPrint = '-';
		lib_USART3_Write(&toPrint, (uint16_t) sizeof(toPrint));
		number *= -1;
	}

	/* Rounding up number (if number = 1.9999, it'll be rounded to 2.0) */
	float rounding = 0.5;
	for(uint8_t i = 0; i < decimalDigits; ++i)
	{
		rounding /= 10.0;
	}
	number += rounding;

	/* Extract positive part and send through UART TX */
	uint32_t integerPart = (uint32_t)number;
	uint8_t data[10];
	int8_t i = 0;
	if(integerPart == 0)
	{
		toPrint = '0';
		lib_USART3_Write(&toPrint, (uint16_t) sizeof(toPrint));
	}
	else
	{
		while(integerPart != 0)
		{
			data[i] = (integerPart % 10);
			integerPart = (integerPart - data[i]) / 10;
			i++;
		}
	}
	for(i--; i > -1; i--)
	{
		toPrint = data[i] + '0';
		lib_USART3_Write(&toPrint, (uint16_t) sizeof(toPrint));
	}

	/* Print decimal dot if needed */
	if(decimalDigits > 0)
	{
		toPrint = '.';
		lib_USART3_Write(&toPrint, (uint16_t) sizeof(toPrint));
	}

	/* Extract decimal digits and send them through USART3 TX */
	integerPart = (uint32_t)number;
	float remainder = number - (float)integerPart;
	while (decimalDigits-- > 0)
	{
		remainder *= 10.0;
		int32_t toPrint2 = (int32_t)(remainder);
		toPrint = toPrint2 + '0';
		lib_USART3_Write(&toPrint, (uint16_t) sizeof(toPrint));
		remainder -= toPrint2;
	}
}

uint8_t lib_USART3_Read(void)
{
	uint8_t dataReceived = 0;

	/* Receive data - wait 10 seconds only or go to Handle Error */
	if(HAL_UART_Receive(&lib_USART3_InitStruct, &dataReceived, (uint16_t)sizeof(dataReceived), (uint32_t)10000U) != HAL_OK)
	{
		/* Handle error */
		lib_USART3_Error_Handler();
	}

	return dataReceived;
}
