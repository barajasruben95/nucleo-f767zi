/**
 * @file    lib_OneWire.h
 * @date    6-September-2023
 * @brief   OneWire library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2023

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software,
    and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef LIB_ONEWIRE_H
#define LIB_ONEWIRE_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "stm32f7xx.h"
#include "lib_Delay.h"

/*
 ***********************
 ***** DEFINITIONS *****
 ***********************
 */

/**
 * @brief  OneWire structure
 */
typedef struct {
	GPIO_TypeDef* gpioOneWire;     /*!< GPIOx port to be used for I/O functions */
	GPIO_TypeDef* gpioStrongPullUp;/*!< GPIOx port to be used for strong pull up */
	uint16_t gpioPinOneWire;       /*!< GPIO Pin to be used for I/O functions */
	uint16_t gpioPinStrongPullUp;  /*!< GPIO Pin to be used for strong pull up */
} OneWire_t;

/**
 * @brief  OneWire status structure
 */
typedef enum {
	ONEWIRE_OK,
	ONEWIRE_NOK
} OneWireStatus_t;

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/

/**
 * @brief  Initialize onewire bus
 * @param  oneWireStruct 	Pointer to @ref OneWire_t structure empty structure
 * @param  gpioOneWire		Pointer to GPIO port used for onewire channel
 * @param  gpioPinOneWire	GPIO Pin on specific GPIOx to be used for onewire channel
 * @retval None
 */
void lib_OneWire_Init(OneWire_t* oneWireStruct, GPIO_TypeDef* gpioOneWire, uint16_t gpioPinOneWire);

/**
 * @brief  Initialize strong pull up line
 * @param  oneWireStruct 		Pointer to @ref OneWire_t structure empty structure
 * @param  gpioStrongPullUp  	Pointer to GPIO port used for strong pull up channel
 * @param  gpioPinStrongPullUp 	GPIO Pin on specific GPIOx to be used for strong pull up channel
 * @retval None
 */
void lib_OneWire_Init_StrongPullUp(OneWire_t* oneWireStruct, GPIO_TypeDef* gpioStrongPullUp, uint16_t gpioPinStrongPullUp);

/**
 * @brief  Reset onewire bus
 * @note   Sends reset command
 * @param  oneWireStruct Pointer to @ref OneWire_t structure structure
 * @retval Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 */
OneWireStatus_t lib_OneWire_Reset(OneWire_t* oneWireStruct);

/**
 * @brief  Write a byte to the onewire bus
 * @param  oneWireStruct 	Pointer to @ref OneWire_t structure structure
 * @param  byte 			8-bit value to write over the onewire protocol
 * @retval None
 */
void lib_OneWire_WriteByte(OneWire_t* oneWireStruct, uint8_t byte);

/**
 * @brief  Read a byte from onewire bus
 * @param  oneWireStruct Pointer to @ref OneWire_t structure structure
 * @retval Byte read from the onewire protocol
 */
uint8_t lib_OneWire_ReadByte(OneWire_t* oneWireStruct);

/**
 * @brief  Start search
 * @note   When you want to search for ALL devices on one onewire port, you should first use this function.
\code
//...Initialization before
status = OneWire_FirstROM(&oneWireStruct);
while (status == ONEWIRE_OK) {
    //Save ROM number from device
    OneWire_GetFullROM(ROM_Array_Pointer);
    //Check for new device
    status = OneWire_NextROM(&oneWireStruct);
}
\endcode
 * @param  oneWireStruct Pointer to @ref OneWire_t structure where to reset search values
 * @retval Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 */
OneWireStatus_t lib_OneWire_FirstROM(OneWire_t* oneWireStruct);

/**
 * @brief  Read next device
 * @note   Use @ref OneWire_First to start searching.
 \code
//...Initialization before
status = OneWire_FirstROM(&oneWireStruct);
while (status == ONEWIRE_OK) {
    //Save ROM number from device
    OneWire_GetFullROM(ROM_Array_Pointer);
    //Check for new device
    status = OneWire_NextROM(&oneWireStruct);
}
\endcode
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @retval Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 */
OneWireStatus_t lib_OneWire_NextROM(OneWire_t* oneWireStruct);

/**
 * @brief  Issue reset pulse and select an specific slave on the bus
 * @param  oneWireStruct	Pointer to @ref OneWire_t structure
 * @param  addr 			Pointer to first location of 8-bytes long ROM address
 * @retval Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 */
OneWireStatus_t lib_OneWire_MatchROM(OneWire_t* oneWireStruct, uint8_t* addr);

/**
 * @brief  Skip ROM addresing. Single onewire device connected to the bus
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @param  addr			 Pointer to first location of 8-bytes long ROM address
 * @param  Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 */
OneWireStatus_t lib_OneWire_SkipROM(OneWire_t* oneWireStruct, uint8_t* addr);

/**
 * @brief  Read ROM from the unique onewire slave on bus
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @param  addr			 Pointer to first location to store address read
 * @retval Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 */
OneWireStatus_t lib_OneWire_ReadROM(OneWire_t* oneWireStruct, uint8_t* addr);

/**
 * @brief  Gets all 8 bytes ROM value from device from search
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @param  firstIndex	 Pointer to first location for first byte, other bytes are automatically incremented
 * @retval None
 */
void lib_OneWire_GetFullROM(OneWire_t* oneWireStruct, uint8_t *firstIndex);

/**
 * @brief  Calculate the 8-bit CRC for 1-wire devices
 * @param  addr		Pointer to 8-bit array of data to calculate CRC
 * @param  len		Number of bytes to check
 * @retval Calculated CRC from input data
 */
uint8_t lib_OneWire_CRC8(uint8_t* addr, uint8_t len);

/**
 * @brief  Disable Strong PullUp line
 * @param  oneWireStruct Pointer to @ref OneWire_t structure structure
 * @retval None
 * @note   Previous strong pull up initialization @ref lib_OneWire_Init_StrongPullUp
 */
void lib_OneWire_StrongPullUpDisable(OneWire_t* oneWireStruct);

/**
 * @brief  Enable Strong PullUp line
 * @param  oneWireStruct Pointer to @ref OneWire_t structure structure
 * @retval None
 * @note   Previous strong pull up initialization @ref lib_OneWire_Init_StrongPullUp
 */
void lib_OneWire_StrongPullUpEnable(OneWire_t* oneWireStruct);

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif
