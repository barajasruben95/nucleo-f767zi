/**
0
 * @file    lib_OneWire.c
 * @date    6-September-2023
 * @brief   OneWire library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "lib_OneWire.h"

/*
 ***********************
 ***** DEFINITIONS *****
 ***********************
 */

/* Onewire definitions */
#define ONEWIRE_DELAY_1       10U    /*!< DELAY IN ms TO INITIALIZE ONEWIRE BUS */
#define ONEWIRE_ROM_LENGTH    8U     /*!< UNIQUE ROM IS 8 BYTES LENGHT IN EACH ONEWIRE DEVICE */

/* Onewire delay definitions in microseconds */
#define ONEWIRE_DELAY_A       10U         /*!< WRITE 1 */
#define ONEWIRE_DELAY_A2      3U          /*!< WRITE 1, READING */
#define ONEWIRE_DELAY_B       55U         /*!< WRITE 1 */
#define ONEWIRE_DELAY_C       65U         /*!< WRITE 0 */
#define ONEWIRE_DELAY_D       5U          /*!< WRITE 0 */
#define ONEWIRE_DELAY_E       10U         /*!< READING */
#define ONEWIRE_DELAY_F       50U         /*!< READING */
#define ONEWIRE_DELAY_H       480U        /*!< RESET */
#define ONEWIRE_DELAY_I       70U         /*!< RESET */
#define ONEWIRE_DELAY_J       410U        /*!< RESET */

/* Onewire commands */
#define ONEWIRE_CMD_SEARCHROM        0xF0U  /*!< IDENTIFY ALL SLAVE DEVICES IN THE ONEWIRE BUS */
#define ONEWIRE_CMD_READROM          0x33U  /*!< READ UNIQUE ROM 64 BIT LENGTH OF A SINGLE ONEWIRE DEVICE CONNECTED */
#define ONEWIRE_CMD_MATCHROM         0x55U  /*!< ALLOW THE BUS MASTER TO ADDRESS AN SPECIFIC SLAVE DEVICE */
#define ONEWIRE_CMD_SKIPROM          0xCCU  /*!< SKIP ALL ADDRESSES ON THE ONEWIRE BUS SIMULTANEOUSLY. SINGLE ONEWIRE DEVICE CONNECTED */
#define ONEWIRE_CMD_ALARMSEARCH      0xECU  /*!< DETERMINE ANY ALARM FLAG ASSERTED IN THE ONEWIRE BUS */

/**
 * @brief  ROM code, 8 bytes format
 *     8 Bit Family code
 *     48 Bit Serial Number
 *     8 Bit CRC
 */
static uint8_t Onewire_romCode[8];            /*!< 8-bytes address of last search device */

static uint8_t Onewire_lastDiscrepancy;       /*!< Search private */
static uint8_t Onewire_lastFamilyDiscrepancy; /*!< Search private */
static uint8_t Onewire_lastDeviceFlag;        /*!< Search private */

/*
 ****************************
 ***** STATIC FUNCTIONS *****
 ****************************
 */

/**
 * @brief  Set onewire line to logic 1
 * @param  gpioOneWire 		Pointer to GPIO port used for onewire channel
 * @param  gpioPinOneWire	GPIO Pin on specific GPIOx to be used for onewire channel
 * @retval None
 */
static void lib_OneWire_SetPinAsInput(GPIO_TypeDef* gpioOneWire, uint16_t gpioPinOneWire)
{
	GPIO_InitTypeDef gpioInitStruct;

	/* Configure pin */
	gpioInitStruct.Pin = gpioPinOneWire;
	gpioInitStruct.Mode = GPIO_MODE_INPUT;
	gpioInitStruct.Pull = GPIO_PULLUP;
	gpioInitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;

	/* Do initialization */
	HAL_GPIO_Init(gpioOneWire, &gpioInitStruct);
}

/**
 * @brief  Clear onewire line to logic 0
 * @param  gpioOneWire		Pointer to GPIO port used for onewire channel
 * @param  gpioPinOneWire	GPIO Pin on specific GPIOx to be used for onewire channel
 * @retval None
 */
static void lib_OneWire_SetPinAsOutput(GPIO_TypeDef* gpioOneWire, uint16_t gpioPinOneWire)
{
	GPIO_InitTypeDef gpioInitStruct;

	/* Ensure output will be low before configuring */
	HAL_GPIO_WritePin(gpioOneWire, gpioPinOneWire, GPIO_PIN_RESET);

	/* Configure pin */
	gpioInitStruct.Pin = gpioPinOneWire;
	gpioInitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	gpioInitStruct.Pull = GPIO_NOPULL;
	gpioInitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;

	/* Do initialization */
	HAL_GPIO_Init(gpioOneWire, &gpioInitStruct);
}

/**
 * @brief  Writes single bit to onewire bus
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @param  bit: Bit value to send, 1 or 0
 * @retval None
 */
static void lib_OneWire_WriteBit(OneWire_t* oneWireStruct, uint8_t bit)
{
	if(bit == 1)
	{
		/* Set line low */
		lib_OneWire_SetPinAsOutput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
		lib_Delay_US(ONEWIRE_DELAY_A);

		/* Bit high */
		lib_OneWire_SetPinAsInput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);

		/* Wait and release the line */
		lib_Delay_US(ONEWIRE_DELAY_B);
		lib_OneWire_SetPinAsInput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
	}
	else
	{
		/* Set line low */
		lib_OneWire_SetPinAsOutput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
		lib_Delay_US(ONEWIRE_DELAY_C);

		/* Bit high */
		lib_OneWire_SetPinAsInput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);

		/* Wait and release the line */
		lib_Delay_US(ONEWIRE_DELAY_D);
		lib_OneWire_SetPinAsInput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
	}
}

/**
 * @brief  Reads single bit from onewire bus
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @retval Bit value:
 *            @arg 0: Bit is low (zero)
 *            @arg 1: Bit is high (one)
 */
static uint8_t lib_OneWire_ReadBit(OneWire_t* oneWireStruct)
{
	uint8_t bit = 0;

	/* Line low */
	lib_OneWire_SetPinAsOutput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
	lib_Delay_US(ONEWIRE_DELAY_A2);

	/* Release line */
	lib_OneWire_SetPinAsInput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
	lib_Delay_US(ONEWIRE_DELAY_E);

	/* Read line value */
	if(HAL_GPIO_ReadPin(oneWireStruct->gpioOneWire, oneWireStruct->gpioPinOneWire))
	{
		/* Bit is HIGH */
		bit = 1;
	}

	/* Wait to complete an especific reading period */
	lib_Delay_US(ONEWIRE_DELAY_F);

	/* Return bit value */
	return bit;
}

/**
 * @brief  Resets search states
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @retval None
 */
static void lib_OneWire_ResetSearch(OneWire_t* oneWireStruct)
{
	/* Reset the search state */
	Onewire_lastDiscrepancy = 0;
	Onewire_lastDeviceFlag = 0;
	Onewire_lastFamilyDiscrepancy = 0;
}

/**
 * @brief  Searches for onewire devices on specific port
 * @param  oneWireStruct Pointer to @ref OneWire_t structure
 * @param  command		 Onewire commands allowed by the protocol
 * @retval Device status:
 *            @arg ONEWIRE_OK at least one device is connected
 *            @arg ONEWIRE_NOK no devices detected in the bus
 * @note   Not meant for public use. Use @ref OneWire_First and @ref OneWire_Next for this.
 */
static OneWireStatus_t lib_OneWire_Search(OneWire_t* oneWireStruct, uint8_t command)
{
	uint8_t id_bit_number;
	uint8_t last_zero, rom_byte_number;
	uint8_t id_bit, cmp_id_bit;
	uint8_t rom_byte_mask, search_direction;
	OneWireStatus_t search_result;

	/* Initialize for search */
	id_bit_number = 1;
	last_zero = 0;
	rom_byte_number = 0;
	rom_byte_mask = 1;
	search_result = ONEWIRE_NOK;

	/* Check if any devices */
	if(!Onewire_lastDeviceFlag)
	{
		/* 1-Wire reset */
		if(lib_OneWire_Reset(oneWireStruct) == ONEWIRE_NOK)
		{
			/* Reset the search */
			lib_OneWire_ResetSearch(oneWireStruct);
			return ONEWIRE_NOK;
		}

		/* Issue the search command */
		lib_OneWire_WriteByte(oneWireStruct, command);

		/* Loop to do the search */
		do
		{
			/* Read a bit and its complement */
			id_bit = lib_OneWire_ReadBit(oneWireStruct);
			cmp_id_bit = lib_OneWire_ReadBit(oneWireStruct);

			/* Check for no devices on 1-wire */
			if((id_bit == 1) && (cmp_id_bit == 1))
			{
				break;
			}
			else
			{
				/* All devices coupled have 0 or 1 */
				if(id_bit != cmp_id_bit)
				{
					/* Bit write value for search */
					search_direction = id_bit;
				}
				else
				{
					/* If this discrepancy is before the Last Discrepancy on a previous next then pick the same as last time */
					if(id_bit_number < Onewire_lastDiscrepancy)
					{
						search_direction = ((Onewire_romCode[rom_byte_number] & rom_byte_mask) > 0);
					}
					else
					{
						/* If equal to last pick 1, if not then pick 0 */
						search_direction = (id_bit_number == Onewire_lastDiscrepancy);
					}

					/* If 0 was picked then record its position in LastZero */
					if(search_direction == 0)
					{
						last_zero = id_bit_number;

						/* Check for Last discrepancy in family */
						if(last_zero < 9)
						{
							Onewire_lastFamilyDiscrepancy = last_zero;
						}
					}
				}

				/* Set or clear the bit in the ROM byte rom_byte_number with mask rom_byte_mask */
				if(search_direction == 1)
				{
					Onewire_romCode[rom_byte_number] |= rom_byte_mask;
				}
				else
				{
					Onewire_romCode[rom_byte_number] &= ~rom_byte_mask;
				}

				/* Serial number search direction write bit */
				lib_OneWire_WriteBit(oneWireStruct, search_direction);

				/* Increment the byte counter id_bit_number and shift the mask rom_byte_mask */
				id_bit_number++;
				rom_byte_mask <<= 1;

				/* If the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask */
				if(rom_byte_mask == 0)
				{
					rom_byte_number++;
					rom_byte_mask = 1;
				}
			}
			/* Loop until through all ROM bytes 0-7 */
		} while(rom_byte_number < ONEWIRE_ROM_LENGTH);

		/* If the search was successful then */
		if(!(id_bit_number < 65))
		{
			/* Search successful so set lastDiscrepancy, lastDeviceFlag, search_result */
			Onewire_lastDiscrepancy = last_zero;

			/* Check for last device */
			if(Onewire_lastDiscrepancy == 0)
			{
				Onewire_lastDeviceFlag = 1;
			}

			search_result = ONEWIRE_OK;
		}
	}

	/* If no device found then reset counters so next 'search' will be like a first */
	if(search_result == ONEWIRE_NOK || !Onewire_romCode[0])
	{
		lib_OneWire_ResetSearch(oneWireStruct);
	}

	return search_result;
}

/**
 * @brief  Enable clock peripheral for given GPIO
 * @param  gpioOneWire: Pointer to GPIO port used for onewire channel
 * @retval None
 */
static void lib_OneWire_EnableGPIOclock (GPIO_TypeDef* gpioOneWire)
{
	if(GPIOA == gpioOneWire)
	{
		__HAL_RCC_GPIOA_CLK_ENABLE();
	}
	else if(GPIOB == gpioOneWire)
	{
		__HAL_RCC_GPIOB_CLK_ENABLE();
	}
	else if(GPIOC == gpioOneWire)
	{
		__HAL_RCC_GPIOC_CLK_ENABLE();
	}
	else if(GPIOD == gpioOneWire)
	{
		__HAL_RCC_GPIOD_CLK_ENABLE();
	}
	else if(GPIOE == gpioOneWire)
	{
		__HAL_RCC_GPIOE_CLK_ENABLE();
	}
	else if(GPIOF == gpioOneWire)
	{
		__HAL_RCC_GPIOF_CLK_ENABLE();
	}
	else if(GPIOG == gpioOneWire)
	{
		__HAL_RCC_GPIOG_CLK_ENABLE();
	}
	else if(GPIOH == gpioOneWire)
	{
		__HAL_RCC_GPIOH_CLK_ENABLE();
	}
	else if(GPIOI == gpioOneWire)
	{
		__HAL_RCC_GPIOI_CLK_ENABLE();
	}
	else if(GPIOJ == gpioOneWire)
	{
		__HAL_RCC_GPIOJ_CLK_ENABLE();
	}
	else /* GPIOK == gpioOneWire */
	{
		__HAL_RCC_GPIOK_CLK_ENABLE();
	}
}

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/

void lib_OneWire_Init(OneWire_t* oneWireStruct, GPIO_TypeDef* gpioOneWire, uint16_t gpioPinOneWire)
{
	/* Initialize delay library */
	lib_Delay_Init();

	/* Enable clock for GPIO */
	lib_OneWire_EnableGPIOclock(gpioOneWire);

	/* Init GPIO pin as input with no pull up resistor */
	lib_OneWire_SetPinAsInput(gpioOneWire, gpioPinOneWire);

	/* Save settings */
	oneWireStruct->gpioOneWire = gpioOneWire;
	oneWireStruct->gpioPinOneWire = gpioPinOneWire;

	lib_Delay_MS(ONEWIRE_DELAY_A);
}

void lib_OneWire_Init_StrongPullUp(OneWire_t* oneWireStruct, GPIO_TypeDef* gpioStrongPullUp, uint16_t gpioPinStrongPullUp)
{
	GPIO_InitTypeDef gpioInitStruct;

	/* Enable clock for GPIO */
	lib_OneWire_EnableGPIOclock(gpioStrongPullUp);

	/* Save settings */
	oneWireStruct->gpioStrongPullUp = gpioStrongPullUp;
	oneWireStruct->gpioPinStrongPullUp = gpioPinStrongPullUp;

	/* Init with Strong PullUp disabled */
	lib_OneWire_StrongPullUpDisable(oneWireStruct);

	/* Configure pin */
	gpioInitStruct.Pin = gpioPinStrongPullUp;
	gpioInitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	gpioInitStruct.Pull = GPIO_NOPULL;
	gpioInitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;

	/* Do initialization */
	HAL_GPIO_Init(gpioStrongPullUp, &gpioInitStruct);
}

OneWireStatus_t lib_OneWire_Reset(OneWire_t* oneWireStruct)
{
	uint8_t presencePulse;

	/* Line low, and wait */
	lib_OneWire_SetPinAsOutput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
	lib_Delay_US(ONEWIRE_DELAY_H);

	/* Release line and wait */
	lib_OneWire_SetPinAsInput(oneWireStruct->gpioOneWire, (oneWireStruct)->gpioPinOneWire);
	lib_Delay_US(ONEWIRE_DELAY_I);

	/* Check for presence pin */
	presencePulse = HAL_GPIO_ReadPin(oneWireStruct->gpioOneWire, oneWireStruct->gpioPinOneWire);

	/* Delay*/
	lib_Delay_US(ONEWIRE_DELAY_J);

	/* Check presence pulse */
	return ((presencePulse == 0) ? ONEWIRE_OK : ONEWIRE_NOK);
}

void lib_OneWire_WriteByte(OneWire_t* oneWireStruct, uint8_t byte)
{
	uint8_t i = 8;

	/* Write 8 bits */
	while(i--)
	{
		/* LSB bit is first */
		lib_OneWire_WriteBit(oneWireStruct, byte & 0x01);
		byte >>= 1;
	}
}

uint8_t lib_OneWire_ReadByte(OneWire_t* oneWireStruct)
{
	uint8_t i = 8, byte = 0;

	while(i--)
	{
		byte >>= 1;
		byte |= (lib_OneWire_ReadBit(oneWireStruct) << 7);
	}

	return byte;
}

OneWireStatus_t lib_OneWire_FirstROM(OneWire_t* oneWireStruct)
{
	/* Reset search values */
	lib_OneWire_ResetSearch(oneWireStruct);

	/* Start with searching */
	return lib_OneWire_Search(oneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

OneWireStatus_t lib_OneWire_NextROM(OneWire_t* oneWireStruct)
{
	/* Leave the search state alone */
	return lib_OneWire_Search(oneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

OneWireStatus_t lib_OneWire_MatchROM(OneWire_t* oneWireStruct, uint8_t* addr)
{
	uint8_t i;
	OneWireStatus_t retVal = ONEWIRE_NOK;

	retVal = lib_OneWire_Reset(oneWireStruct);
	if(retVal == ONEWIRE_OK)
	{
		lib_OneWire_WriteByte(oneWireStruct, ONEWIRE_CMD_MATCHROM);

		for (i = 0; i < ONEWIRE_ROM_LENGTH; i++)
		{
			lib_OneWire_WriteByte(oneWireStruct, *(addr + i));
		}
	}

	return retVal;
}

OneWireStatus_t lib_OneWire_SkipROM(OneWire_t* oneWireStruct, uint8_t* addr)
{
	OneWireStatus_t retVal;

	retVal = lib_OneWire_Reset(oneWireStruct);
	if(retVal == ONEWIRE_OK)
	{
		lib_OneWire_WriteByte(oneWireStruct, ONEWIRE_CMD_SKIPROM);
	}

	return retVal;
}

OneWireStatus_t lib_OneWire_ReadROM(OneWire_t* oneWireStruct, uint8_t* addr)
{
	uint8_t i;
	OneWireStatus_t retVal;

	retVal = lib_OneWire_Reset(oneWireStruct);
	if(retVal == ONEWIRE_OK)
	{
		lib_OneWire_WriteByte(oneWireStruct, ONEWIRE_CMD_READROM);

		for (i = 0; i < ONEWIRE_ROM_LENGTH; i++)
		{
			*(addr + i) = lib_OneWire_ReadByte(oneWireStruct);
		}
	}

	return retVal;
}

void lib_OneWire_GetFullROM(OneWire_t* oneWireStruct, uint8_t *firstIndex)
{
	uint8_t i;

	for (i = 0; i < ONEWIRE_ROM_LENGTH; i++)
	{
		*(firstIndex + i) = Onewire_romCode[i];
	}
}

uint8_t lib_OneWire_CRC8(uint8_t *addr, uint8_t len)
{
	uint8_t crc = 0, inbyte, i, mix;

	while(len--)
	{
		inbyte = *addr++;
		for(i = 8; i; i--)
		{
			mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if(mix == 1)
			{
				crc ^= 0x8C;
			}
			inbyte >>= 1;
		}
	}

	/* Return calculated CRC */
	return crc;
}

void lib_OneWire_StrongPullUpDisable(OneWire_t* oneWireStruct)
{
	HAL_GPIO_WritePin(oneWireStruct->gpioStrongPullUp, oneWireStruct->gpioPinStrongPullUp, GPIO_PIN_RESET);
}

void lib_OneWire_StrongPullUpEnable(OneWire_t* oneWireStruct)
{
	HAL_GPIO_WritePin(oneWireStruct->gpioStrongPullUp, oneWireStruct->gpioPinStrongPullUp, GPIO_PIN_SET);
}
