/**
 ******************************************************************************
 * @file    classLED.hpp
 * @author  Ruben Barajas
 * @version V1.0
 * @date    13-September-2023
 * @brief   HEADER - Manage BSP LEDs through c++ language
 ******************************************************************************
 */

#ifndef CLASSLED_HPP_
#define CLASSLED_HPP_

#include "stm32f7xx.h"
#include "stm32f7xx_nucleo_144.h"

class classLED
{
private:
    Led_TypeDef ledInitialized; /// Stores the LED used
    uint8_t ledStatus; //// Stores if LED is ON or OFF
    void init(Led_TypeDef ledInitialized);

public:
    classLED();
    classLED(Led_TypeDef ledInitialized);
    ~classLED();
    void setStatus(uint8_t status);
    void toogleStatus();
    uint8_t getStatus();
};

#endif /* CLASSLED_HPP_ */
