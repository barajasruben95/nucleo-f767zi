/**
 ******************************************************************************
 * @file    classLED.cpp
 * @author  Ruben Barajas
 * @version V1.0
 * @date    13-September-2023
 * @brief   Manage BSP LEDs through c++ language
 ******************************************************************************
 */

#include "classLED.hpp"

classLED::classLED()
{
    /* Call function to initialize LED RED*/
    init(LED_RED);
}

classLED::classLED(Led_TypeDef ledInitialized)
{
    /* Call function to initialize LED */
    init(ledInitialized);
}

classLED::~classLED()
{
    /* Destructor definition, de-initialize LED */
    BSP_LED_DeInit(this->ledInitialized);
}

void classLED::setStatus(uint8_t status)
{
    this->ledStatus = status;
    if (0 == status)
    {
        BSP_LED_Off(this->ledInitialized);
    }
    else
    {
        BSP_LED_On(this->ledInitialized);
    }
}

void classLED::toogleStatus()
{
    BSP_LED_Toggle(this->ledInitialized);
    this->ledStatus ^= 1;
}

uint8_t classLED::getStatus()
{
    return this->ledStatus;
}

void classLED::init(Led_TypeDef ledInitialized)
{
    this->ledStatus = 0;
    this->ledInitialized = ledInitialized;

    BSP_LED_Init(this->ledInitialized);
}
