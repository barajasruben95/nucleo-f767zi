/**
 ******************************************************************************
 * @file    main.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    29-March-2021
 * @brief   Default main function.
 ******************************************************************************
 */

#include "stm32f7xx.h"
#include "stm32f7xx_nucleo_144.h"
#include "arm_math.h"
#include <math.h>

#define MAX_BLOCKSIZE    32
#define DELTA            (0.000001f)

/* Test input data of srcA for blockSize 32 */
float32_t srcA_buf_f32[MAX_BLOCKSIZE] =
{
        -0.4325648115282207,  -1.6655843782380970,  0.1253323064748307,
        0.2876764203585489,  -1.1464713506814637,  1.1909154656429988,
        1.1891642016521031,  -0.0376332765933176,  0.3272923614086541,
        0.1746391428209245,  -0.1867085776814394,  0.7257905482933027,
        -0.5883165430141887,   2.1831858181971011, -0.1363958830865957,
        0.1139313135208096,   1.0667682113591888,  0.0592814605236053,
        -0.0956484054836690,  -0.8323494636500225,  0.2944108163926404,
        -1.3361818579378040,   0.7143245518189522,  1.6235620644462707,
        -0.6917757017022868,   0.8579966728282626,  1.2540014216025324,
        -1.5937295764474768,  -1.4409644319010200,  0.5711476236581780,
        -0.3998855777153632,   0.6899973754643451
};

/* Test input data of srcB for blockSize 32 */
float32_t srcB_buf_f32[MAX_BLOCKSIZE] =
{
        1.7491401329284098,  0.1325982188803279,   0.3252281811989881,
        -0.7938091410349637,  0.3149236145048914,  -0.5272704888029532,
        0.9322666565031119,  1.1646643544607362,  -2.0456694357357357,
        -0.6443728590041911,  1.7410657940825480,   0.4867684246821860,
        1.0488288293660140,  1.4885752747099299,   1.2705014969484090,
        -1.8561241921210170,  2.1343209047321410,   1.4358467535865909,
        -0.9173023332875400, -1.1060770780029008,   0.8105708062681296,
        0.6985430696369063, -0.4015827425012831,   1.2687512030669628,
        -0.7836083053674872,  0.2132664971465569,   0.7878984786088954,
        0.8966819356782295, -0.1869172943544062,   1.0131816724341454,
        0.2484350696132857,  0.0596083377937976
};

/* Reference dot product output */
float32_t refDotProdOut = 5.9273644806352142;

int main(void)
{
    /* Intermediate output */
    float32_t multOutput[MAX_BLOCKSIZE] = {0};

    /* Final output */
    float32_t testOutput1 = 0;
    float32_t testOutput2 = 0;

    /* Status of the example */
    arm_status status1 = ARM_MATH_TEST_FAILURE;
    arm_status status2 = ARM_MATH_TEST_FAILURE;

    /* Difference between reference and test outputs */
    float32_t diff1 = 0;
    float32_t diff2 = 0;

    /* ******************** */
    /* ***** CALCULUS ***** */
    /* ******************** */

    /* Multiplication of two input buffers */
    arm_mult_f32(srcA_buf_f32, srcB_buf_f32, multOutput, MAX_BLOCKSIZE);

    /* Accumulate the multiplication output values to get the dot product of the two inputs */
    for(uint32_t i = 0; i <  MAX_BLOCKSIZE; i++)
    {
        arm_add_f32(&testOutput1, &multOutput[i], &testOutput1, 1);
    }

    /* Absolute value of difference between ref and test */
    diff1 = fabsf(refDotProdOut - testOutput1);

    /* Comparison of dot product value with reference */
    status1 = (diff1 > DELTA) ? ARM_MATH_TEST_FAILURE : ARM_MATH_SUCCESS;

    /* ***************** */
    /* ***** CMSIS ***** */
    /* ***************** */
    arm_dot_prod_f32(srcA_buf_f32, srcB_buf_f32, MAX_BLOCKSIZE, &testOutput2);

    /* Absolute value of difference between ref and test */
    diff2 = fabsf(refDotProdOut - testOutput2);

    /* Comparison of dot product value with reference */
    status2 = (diff2 > DELTA) ? ARM_MATH_TEST_FAILURE : ARM_MATH_SUCCESS;

    /* Check with debugger that both diff1 and diff2 have passed successfully */
    for(;;);
}
