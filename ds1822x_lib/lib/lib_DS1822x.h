/**
 * @file    lib_DS1822x.h
 * @date    17-October-2023
 * @brief   DS1822 and DS1822P library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2023

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software,
    and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */
#ifndef lib_DS1822X_H
#define lib_DS1822X_H

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "stm32f7xx.h"
#include "lib_OneWire.h"

/*
 ***********************
 ***** DEFINITIONS *****
 ***********************
 */

/**
 * @brief  DS1822x status structure
 */
typedef enum {
	DS1822x_OK,         /*!< No errors detected in the DS1822x device */
	DS1822x_NOK,        /*!< Operation failed in the DS1822x device, it's not connected to the onewire bus */
	DS1822x_CRC_ERROR,  /*!< CRC error, data read VS data in the DS1822x does not match */
	DS1822x_TIMEOUT     /*!< Timeout when copying data from EEPROM memory to scratchpad */
} DS1822xStatus_t;

/**
 * @brief  DS1822x temperature resolution structure
 */
typedef enum {
	DS1822x_TMP_RES_9 = 9U,    /*!< Temperature resolution of 9 bits */
	DS1822x_TMP_RES_10,        /*!< Temperature resolution of 10 bits */
	DS1822x_TMP_RES_11,        /*!< Temperature resolution of 11 bits */
	DS1822x_TMP_RES_12         /*!< Temperature resolution of 12 bits */
} DS1822xTmpRes_t;

/**
 * @brief  DS1822x structure
 */
typedef struct
{
	float temperature;            /*!< Temperature read by the device */
	uint8_t rom_code[8];          /*!< Unique 8 bytes ROM code */
	int8_t tempLimitHigh;         /*!< High temperature limit to send an alert */
	int8_t tempLimitLow;          /*!< Low temperature limit to send an alert */
	DS1822xTmpRes_t resolution;   /*!< Temperature resolution: 9, 10, 11 or 12 bits */
	OneWire_t oneWireStruct;      /*!< OneWire struct where the DS1822x is connected */
} DS1822x_t;

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/

/**
 * @brief  Initialize the DS1822P device connected to the onewire bus
 * @param  ds1822xStruct		Pointer to @ref DS1822x_t structure
 * @param  OneWire_t 			Pointer to @ref OneWire_t onewire structure
 * @param  romAddress 			Unique 8 bytes ROM of the DS1822P to communicate
 * @param  gpioStrongPullUp 	Pointer to GPIO port used for strong pull up channel
 * @param  gpioPinStrongPullUp 	GPIO Pin on specific GPIOx to be used for strong pull up channel
 * @retval Status of the temperature conversion, ref @DS1822xStatus_t
 *             @arg DS1822P_OK temperature converted successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822P_CRC_ERROR temperature converted has CRC error
 */
DS1822xStatus_t lib_DS1822P_Init(DS1822x_t* ds1822xStruct, OneWire_t* oneWireStruct, uint8_t* romAddress, GPIO_TypeDef* gpioStrongPullUp, uint16_t gpioPinStrongPullUp);

/**
 * @brief  Initialize the DS1822 device connected to the onewire bus
 * @param  ds1822xStruct 		Pointer to @ref DS1822x_t structure
 * @param  OneWire_t 			Pointer to @ref OneWire_t onewire structure
 * @param  romAddress 			Unique 8 bytes ROM of the DS1822P to communicate
 * @param  gpioStrongPullUp 	Pointer to GPIO port used for strong pull up channel
 * @param  gpioPinStrongPullUp 	GPIO Pin on specific GPIOx to be used for strong pull up channel
 * @retval Status of the temperature conversion, ref @DS1822xStatus_t
 *             @arg DS1822P_OK temperature converted successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822P_CRC_ERROR temperature converted has CRC error
 */
DS1822xStatus_t lib_DS1822_Init(DS1822x_t* ds1822xStruct, OneWire_t* oneWireStruct, uint8_t* romAddress);

/**
 * @brief  Read temperature of the DS1822P sensor
 * @param  ds1822xStruct Pointer to @ref DS1822x_t structure
 * @retval Status of the temperature conversion, ref @DS1822xStatus_t
 *             @arg DS1822P_OK temperature converted successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822P_CRC_ERROR temperature converted has CRC error
 */
DS1822xStatus_t lib_DS1822P_ReadTemperature(DS1822x_t* ds1822xStruct);

/**
 * @brief  Read temperature of the sensor DS1822
 * @param  ds1822xStruct Pointer to @ref DS1822x_t structure
 * @retval Status of the temperature conversion, ref @DS1822xStatus_t
 *             @arg DS1822P_OK temperature converted successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822P_CRC_ERROR temperature converted has CRC error
 */
DS1822xStatus_t lib_DS1822_ReadTemperature(DS1822x_t* ds1822xStruct);

/**
 * @brief  Get the temperature limits and temperature resolution from the DS1822x device
 * @param  ds1822xStruct Pointer to @ref DS1822x_t structure
 * @retval Status of the operation ref @DS1822xStatus_t
 *             @arg DS1822P_OK recall completed successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822x_tIMEOUT data can't be read from EEPROM
 * @note temperature limits and temperature resolution configurations values read from the EEPROM are placed
 *       in tempLimitHigh, tempLimitLow and resolution attributes from ds1822xStruct @ref DS1822x_t
 */
DS1822xStatus_t lib_DS1822x_GetConfig(DS1822x_t* ds1822xStruct);

/**
 * @brief  Set the temperature limits and temperature resolution for the DS1822P device in the EEPROM memory
 * @param  ds1822xStruct 	Pointer to @ref DS1822x_t structure
 * @param  highLimTemp 		Value for the high temperature limit in TH register
 * @param  lowLimTemp 		Value for the low temperature limit in TL register
 * @param  sensorResolution Temperature resolution to the DS1822P sensor
 * @retval Status of the operation ref @DS1822xStatus_t
 *             @arg DS1822P_OK configuration set successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 */
DS1822xStatus_t lib_DS1822P_SetConfig(DS1822x_t* ds1822xStruct, int8_t highLimTemp, int8_t lowLimTemp, DS1822xTmpRes_t sensorResolution);

/**
 * @brief  Set the temperature limits and temperature resolution for the DS1822 device in the EEPROM memory
 * @param  ds1822xStruct 	Pointer to @ref DS1822x_t structure
 * @param  highLimTemp 		Value for the high temperature limit in TH register
 * @param  lowLimTemp 		Value for the low temperature limit in TL register
 * @param  sensorResolution Temperature resolution to the DS1822P sensor
 * @retval Status of the operation ref @DS1822xStatus_t
 *             @arg DS1822P_OK configuration set successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 */
DS1822xStatus_t lib_DS1822_SetConfig(DS1822x_t* ds1822xStruct, int8_t highLimTemp, int8_t lowLimTemp, DS1822xTmpRes_t sensorResolution);

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif
