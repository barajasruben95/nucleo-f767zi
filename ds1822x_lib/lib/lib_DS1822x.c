/**
 * @file    lib_DS1822x.c
 * @date    17-October-2023
 * @brief   DS1822x library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "lib_DS1822x.h"

/*
 ***********************
 ***** DEFINITIONS *****
 ***********************
 */

/* DS1822 definitions */
#define DS1822x_TMP_SIGN_MASK                0x8000U        /*!< TEMPERATURE SIGN BIT IS THE MSB OF AN uint16_t VARIABLE */
#define DS1822x_DELAY_COPY_SCRATCHPAD        15U            /*!< DELAY TO COPY SCRATCHPAD (TH, TL AND CFG) TO EEPROM */
#define DS1822x_ROM_LENGTH                   8U             /*!< DS1822x ROM LENGTH IS 8 BYTES */
#define DS1822x_EEPROM2SCRATCHPAD_TIMEOUT    10             /*!< DS1822x TIMEOUT IN ms TO COPY DATA FROM EEPROM TO SCRATCHPAD */

/* DS1822x scratchpad definitions */
#define DS1822x_SCRATCHPAD_TOTAL_LENGTH        9U       /*!< SCRATCHPAD TOTAL LENGTH IS 9 BYTES */
#define DS1822x_SCRATCHPAD_DATA_LENGTH         8U       /*!< SCRATCHPAD DATA LENGTH IS 8 BYTES. LAST BYTE IS CRC */
#define DS1822x_SCRATCHPAD_WRITE_LENGTH        3U       /*!< SCRATCHPAD WRITE LENGTH IS 3 BYTES, TH, TL AND CONFIG */

#define DS1822x_SCRATCHPAD_TMP_LSB             0U       /*!< SCRATCHPAD TEMPERATURE LSB REGISTER */
#define DS1822x_SCRATCHPAD_TMP_MSB             1U       /*!< SCRATCHPAD TEMPERATURE MSB REGISTER */
#define DS1822x_SCRATCHPAD_TH                  2U       /*!< SCRATCHPAD TH REGISTER */
#define DS1822x_SCRATCHPAD_TL                  3U       /*!< SCRATCHPAD TL REGISTER */
#define DS1822x_SCRATCHPAD_CONFIG              4U       /*!< SCRATCHPAD CONFIG REGISTER */
#define DS1822x_SCRATCHPAD_CRC                 8U       /*!< SCRATCHPAD CRC REGISTER */

#define DS1822x_SCRATCHPAD_CONFIG_MASK         0x60U    /*!< SCRATCHPAD CONFIG REGISTER MASK */
#define DS1822x_SCRATCHPAD_CONFIG_RSHIFT       5U       /*!< SCRATCHPAD CONFIG REGISTER RIGHT SHIFT */

/* DS1822x Temperature multiplier */
#define DS1822_TMP_DECIMAL_STEPS_9BITS        0.5f
#define DS1822_TMP_DECIMAL_STEPS_10BITS       0.25f
#define DS1822_TMP_DECIMAL_STEPS_11BITS       0.125f
#define DS1822_TMP_DECIMAL_STEPS_12BITS       0.0625f

/* DS1822x temperature conversion delay in miliseconds */
#define DS1822x_DELAY_TMP_RESOLUTION_9BITS        94U     /*!< FROM DATASHEET, 93.75ms WITH 9BITS RESOLUTION */
#define DS1822x_DELAY_TMP_RESOLUTION_10BITS       188U    /*!< FROM DATASHEET, 187.5ms WITH 9BITS RESOLUTION */
#define DS1822x_DELAY_TMP_RESOLUTION_11BITS       375U    /*!< FROM DATASHEET, 375ms WITH 9BITS RESOLUTION */
#define DS1822x_DELAY_TMP_RESOLUTION_12BITS       750U    /*!< FROM DATASHEET, 750 WITH 9BITS RESOLUTION */

/* DS1822x commands */
#define DS1822x_CMD_CONVERT_TEMPERATURE     0x44U   /*!< TEMPERATURE CONVERSION */
#define DS1822x_CMD_WRITE_SCRATCHPAD        0x4EU   /*!< WRITE TH, TL AND CONFIG REGISTERS OF SCRATCHPAD */
#define DS1822x_CMD_READ_SCRATCHPAD         0xBEU   /*!< READ TH, TL, CONFIG AND CRC REGISTERS FROM SCRATCHPAD */
#define DS1822x_CMD_COPY_SCRATCHPAD         0x48U   /*!< COPY TH, TL AND CONFIG REGISTERS FROM SCRATCHPAD TO EEPROM MEMORY */
#define DS1822x_CMD_RECALL_EEPROM           0xB8U   /*!< COPY TH, TL AND CONFIG REGISTERS FROM EEPROM MEMORY TO SCRATHPAD */

/* DS1822x Temperature limits */
#define DS1822x_TMP_HIGH_LIMIT     100   /*!< DS1822x HIGH LIMIT TEMPERATURE IS 100 DEGREES */
#define DS1822x_TMP_LOW_LIMIT     -55    /*!< DS1822x LOW LIMIT TEMPERATURE IS -55 DEGREES */
#define DS1822x_TMP_DEFAULT        85.0f /*!< DS1822x DEFAULT TEMPERATURE AFTER POWER ON */

/**
 * @brief  Scratchpad memory, 9 bytes
 *     Temperature LSB
 *     Temperature MSB
 *     TH register
 *     TL register
 *     Configuration register
 *     Reserved
 *     Reserved
 *     Reserved
 *     CRC
 */
static uint8_t DS1822x_scratchpad[9];

/**
 * @brief  Devices allowed by the library: DS1822 and DS1822P
 */
typedef enum {
	DEVICE_DS1822,         /*!< Device selected is DS1822 */
	DEVICE_DS1822P         /*!< Device selected is DS1822P */
} DS1822xDevice_t;

/*
 ****************************
 ***** STATIC FUNCTIONS *****
 ****************************
 */

/**
 * @brief  Convert temperature
 * @param  ds1822xStruct 	Pointer to @ref DS1822x_t structure
 * @param  device 			DS1822 or DS1822P
 * @retval None
 */
static void lib_DS1822x_ConvertTemperature(DS1822x_t* ds1822xStruct, DS1822xDevice_t device)
{
	/* Convert temperature command */
	lib_OneWire_WriteByte(&ds1822xStruct->oneWireStruct, DS1822x_CMD_CONVERT_TEMPERATURE);

	if(device == DEVICE_DS1822P)
	{
		/* Sensor with parasite mode */
		/* Enable strong pull up during temperature conversion */
		lib_OneWire_StrongPullUpEnable(&ds1822xStruct->oneWireStruct);
	}

	if(ds1822xStruct->resolution == DS1822x_TMP_RES_9)
	{
		lib_Delay_MS(DS1822x_DELAY_TMP_RESOLUTION_9BITS);
	}
	else if(ds1822xStruct->resolution == DS1822x_TMP_RES_10)
	{
		lib_Delay_MS(DS1822x_DELAY_TMP_RESOLUTION_10BITS);
	}
	else if(ds1822xStruct->resolution == DS1822x_TMP_RES_11)
	{
		lib_Delay_MS(DS1822x_DELAY_TMP_RESOLUTION_11BITS);
	}
	else
	{
		/* ds1822xStruct->resolution == DS1822x_TMP_RESOLUTION_12BITS */
		lib_Delay_MS(DS1822x_DELAY_TMP_RESOLUTION_12BITS);
	}

	if(device == DEVICE_DS1822P)
	{
		/* Sensor with parasite mode */
		/* Disable strong pull up after temperature conversion */
		lib_OneWire_StrongPullUpDisable(&ds1822xStruct->oneWireStruct);
	}
}

/**
 * @brief  Read the entire scratchpad (TMPL, TMPH, TH, TL, CFG, RESERVED and CRC) registers and check the CRC of the reading
 * @param  ds1822xStruct Pointer to @ref DS1822x_t structure
 * @retval Status of the operation ref @DS1822xStatus_t
 *             @arg DS1822x_OK read scratchpad completed successfully
 *             @arg DS1822x_CRC_ERROR temperature converted has CRC error
 */
static DS1822xStatus_t lib_DS1822x_ReadScratchpad(DS1822x_t* ds1822xStruct)
{
	uint8_t i;
	DS1822xStatus_t retVal = DS1822x_CRC_ERROR;

	/* Read scratchpad command */
	lib_OneWire_WriteByte(&ds1822xStruct->oneWireStruct, DS1822x_CMD_READ_SCRATCHPAD);

	/* Read and storage, total scratchpad data length bytes */
	for(i = 0; i < DS1822x_SCRATCHPAD_TOTAL_LENGTH; i++)
	{
		DS1822x_scratchpad[i] = lib_OneWire_ReadByte(&ds1822xStruct->oneWireStruct);
	}

	/* Check CRC */
	if(lib_OneWire_CRC8(&DS1822x_scratchpad[0], DS1822x_SCRATCHPAD_DATA_LENGTH) == DS1822x_scratchpad[DS1822x_SCRATCHPAD_CRC])
	{
		retVal = DS1822x_OK;
	}

	return retVal;
}

/**
 * @brief  Write specific values to TH, TL and CONFIG registers in scratchpad
 * @param  ds1822xStruct 	Pointer to @ref DS1822x_t structure
 * @param  data 			TH, TL and CONFIG values in array format
 * @retval None
 */
static void lib_DS1822x_WriteScratchpad(DS1822x_t* ds1822xStruct, uint8_t* data)
{
	uint8_t i;

	/* Write scratchpad command */
	lib_OneWire_WriteByte(&ds1822xStruct->oneWireStruct, DS1822x_CMD_WRITE_SCRATCHPAD);

	/* Write TH, TL and CONFIG registers */
	for(i = 0; i < DS1822x_SCRATCHPAD_WRITE_LENGTH; i++)
	{
		lib_OneWire_WriteByte(&ds1822xStruct->oneWireStruct, *(data + i));
	}
}

/**
 * @brief  Copy the content of the scratchpad TH, TL and CONFIG registers to EEPROM
 * @param  ds1822xStruct 	Pointer to @ref DS1822x_t structure
 * @param  device 			DS1822 or DS1822P
 * @retval None
 */
static void lib_DS1822x_CopyScratchpad(DS1822x_t* ds1822xStruct, DS1822xDevice_t device)
{
	/* Write scratchpad command */
	lib_OneWire_WriteByte(&ds1822xStruct->oneWireStruct, DS1822x_CMD_COPY_SCRATCHPAD);

	if(device == DEVICE_DS1822P)
	{
		/* Sensor with parasite mode */
		/* Enable strong pull up while copying data to EEPROM */
		lib_OneWire_StrongPullUpEnable(&ds1822xStruct->oneWireStruct);
	}

	lib_Delay_MS(DS1822x_DELAY_COPY_SCRATCHPAD);

	if(device == DEVICE_DS1822P)
	{
		/* Sensor with parasite mode */
		/* Disable strong pull up after copying data to EEPROM */
		lib_OneWire_StrongPullUpDisable(&ds1822xStruct->oneWireStruct);
	}
}

/**
 * @brief  Recall the content of TH, TL and CONFIG registers from EEPROM to scratchpad
 * @param  ds1822xStruct Pointer to @ref DS1822x_t structure
 * @retval Status of the operation ref @DS1822xStatus_t
 *             @arg DS1822x_OK recall completed successfully
 *             @arg DS1822x_TIMEOUT data can't be read/copied from EEPROM to scratchpad
 */
static DS1822xStatus_t lib_DS1822x_RecallScratchpad(DS1822x_t* ds1822xStruct)
{
	uint8_t recallStatus;
	DS1822xStatus_t retVal = DS1822x_TIMEOUT;
	uint8_t delay = DS1822x_EEPROM2SCRATCHPAD_TIMEOUT;

	/* Recall EEPROM data to scratchpad command */
	lib_OneWire_WriteByte(&ds1822xStruct->oneWireStruct, DS1822x_CMD_RECALL_EEPROM);

	/* Recall status is seen in onewire bus */
	while(delay > 0)
	{
		/* Read onewire bus */
		recallStatus = lib_OneWire_ReadByte(&ds1822xStruct->oneWireStruct);
		if(recallStatus != 0xFF)
		{
			/* While master reads 0s, busy. Wait 1ms */
			lib_Delay_MS(1);
			delay--;
		}
		else
		{
			/* Recall is complete, finish */
			retVal = DS1822x_OK;
			break;
		}
	}

	return retVal;
}

/**
 * @brief  Initialize the temperature sensor device connected to the onewire bus
 * @param  ds1822xStruct		Pointer to @ref DS1822x_t structure
 * @param  OneWire_t 			Pointer to @ref OneWire_t one wire structure
 * @param  romAddress 			Unique 8 bytes ROM of the DS1822P to communicate
 * @param  gpioStrongPullUp 	Pointer to GPIO port used for strong pull up channel
 * @param  gpioPinStrongPullUp 	GPIO Pin on specific GPIOx to be used for strong pull up channel
 * @param  device 				DS1822 or DS1822P
 * @retval Status of the temperature conversion, ref @DS1822xStatus_t
 *             @arg DS1822P_OK temperature converted successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822P_CRC_ERROR temperature converted has CRC error
 */
static DS1822xStatus_t lib_DS1822x_Init(DS1822x_t* ds1822xStruct, OneWire_t* oneWireStruct, uint8_t* romAddress, GPIO_TypeDef* gpioStrongPullUp, uint16_t gpioPinStrongPullUp, DS1822xDevice_t device)
{
	uint8_t i;
	OneWireStatus_t status = ONEWIRE_NOK;
	DS1822xStatus_t retVal = DS1822x_NOK;

	/* Assign onewire bus to the DS1822x device */
	ds1822xStruct->oneWireStruct = (OneWire_t)*oneWireStruct;

	/* Assign address of the DS1822x device desired */
	for(i = 0; i < DS1822x_ROM_LENGTH; i++)
	{
		ds1822xStruct->rom_code[i] = romAddress[i];
	}

	if(device == DEVICE_DS1822P)
	{
		/* Sensor with parasite mode */
		/* Parasite sensor needs strong pull up initialization */
		lib_OneWire_Init_StrongPullUp(&ds1822xStruct->oneWireStruct, gpioStrongPullUp, gpioPinStrongPullUp);
	}

	/* Read initial DS1822x configuration */

	/* Select DS1822x from the OneWire bus */
	status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
	if(status == ONEWIRE_OK)
	{
		/* Read scratchpad form DS1822x */
		if(lib_DS1822x_ReadScratchpad(ds1822xStruct) != DS1822x_CRC_ERROR)
		{
			/* No CRC issues, reading is correct */

			/* Initialize with default temperature (0x50 LSB and 0x05 MSB -> 0x0550 = 85 degrees) */
			ds1822xStruct->temperature = DS1822x_TMP_DEFAULT;

			/* Temperature limits */
			ds1822xStruct->tempLimitHigh = (int8_t)DS1822x_scratchpad[DS1822x_SCRATCHPAD_TH];
			ds1822xStruct->tempLimitLow = (int8_t)DS1822x_scratchpad[DS1822x_SCRATCHPAD_TL];

			/* Temperature resolution analysis */
			ds1822xStruct->resolution = ((DS1822x_scratchpad[DS1822x_SCRATCHPAD_CONFIG] & DS1822x_SCRATCHPAD_CONFIG_MASK) >> DS1822x_SCRATCHPAD_CONFIG_RSHIFT) + (uint8_t)DS1822x_TMP_RES_9;

			retVal = DS1822x_OK;
		}
		else
		{
			retVal = DS1822x_CRC_ERROR;
		}
	}

	return retVal;
}

/**
 * @brief  Read temperature of the sensor
 * @param  ds1822xStruct 	Pointer to @ref DS1822x_t structure
 * @param  device 			DS1822 or DS1822P
 * @retval Status of the temperature conversion, ref @DS1822xStatus_t
 *             @arg DS1822P_OK temperature converted successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 *             @arg DS1822P_CRC_ERROR temperature converted has CRC error
 */
static DS1822xStatus_t lib_DS1822x_ReadTemperature(DS1822x_t* ds1822xStruct, DS1822xDevice_t device)
{
	uint16_t temperature;
	uint8_t integer;
	uint8_t sign = 0;
	OneWireStatus_t status = ONEWIRE_NOK;
	DS1822xStatus_t retVal = DS1822x_NOK;

	/* Select DS1822x */
	status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
	if(status == ONEWIRE_OK)
	{
		/* Convert temperature */
		lib_DS1822x_ConvertTemperature(ds1822xStruct, device);

		/* Select DS1822x again */
		status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
		if(status == ONEWIRE_OK)
		{
			/* Read scratchpad */
			retVal = lib_DS1822x_ReadScratchpad(ds1822xStruct);
			if(retVal == DS1822x_OK)
			{
				/* First 2 bytes contain temperature */
				temperature = DS1822x_scratchpad[DS1822x_SCRATCHPAD_TMP_LSB] | (DS1822x_scratchpad[DS1822x_SCRATCHPAD_TMP_MSB] << 8);

				/* Check for negative temperature, MSB */
				if ((temperature & DS1822x_TMP_SIGN_MASK) != 0)
				{
					/* two's complement, temperature is negative */
					temperature = ~temperature + 1;
					sign = 1;
				}

				/* Store integer part of the temperature */
				integer = temperature >> 4;
				integer |= ((temperature >> 8) & 0x07) << 4;

				/* Temperature resolution analysis */
				ds1822xStruct->resolution = ((DS1822x_scratchpad[DS1822x_SCRATCHPAD_CONFIG] & DS1822x_SCRATCHPAD_CONFIG_MASK) >> DS1822x_SCRATCHPAD_CONFIG_RSHIFT) + (uint8_t)DS1822x_TMP_RES_9;

				/* Calculate decimal portion */
				if(ds1822xStruct->resolution == DS1822x_TMP_RES_9)
				{
					ds1822xStruct->temperature = (temperature >> 3) & 0x01;
					ds1822xStruct->temperature *= (float)DS1822_TMP_DECIMAL_STEPS_9BITS;
				}
				else if(ds1822xStruct->resolution == DS1822x_TMP_RES_10)
				{
					ds1822xStruct->temperature = (temperature >> 2) & 0x03;
					ds1822xStruct->temperature *= (float)DS1822_TMP_DECIMAL_STEPS_10BITS;
				}
				else if(ds1822xStruct->resolution == DS1822x_TMP_RES_11)
				{
					ds1822xStruct->temperature = (temperature >> 1) & 0x07;
					ds1822xStruct->temperature *= (float)DS1822_TMP_DECIMAL_STEPS_11BITS;
				}
				else
				{
					/* DS1822x_TMP_RESOLUTION_12BITS */
					ds1822xStruct->temperature = temperature & 0x0F;
					ds1822xStruct->temperature *= (float)DS1822_TMP_DECIMAL_STEPS_12BITS;
				}

				/* Final arrangement. Integer + decimal parts and sign */
				ds1822xStruct->temperature = integer + ds1822xStruct->temperature;
				if(sign == 1)
				{
					ds1822xStruct->temperature = -1 * ds1822xStruct->temperature;
				}
			}
		} /* Select DS1822x again */
	} /* Select DS1822x */

	return retVal;
}

/**
 * @brief  Set the temperature limits and temperature resolution for the device in the EEPROM memory
 * @param  ds1822xStruct Pointer to @ref DS1822x_t structure
 * @param  highLimTemp value for the high temperature limit in TH register
 * @param  lowLimTemp value for the low temperature limit in TL register
 * @param  sensorResolution temperature resolution to the DS1822P sensor
 * @param  device DS1822 or DS1822P
 * @retval Status of the operation ref @DS1822xStatus_t
 *             @arg DS1822P_OK configuration set successfully
 *             @arg DS1822P_NOK no device detected in the onewire bus
 */
static DS1822xStatus_t lib_DS1822x_SetConfig(DS1822x_t* ds1822xStruct, int8_t highLimTemp, int8_t lowLimTemp, DS1822xTmpRes_t sensorResolution, DS1822xDevice_t device)
{
	OneWireStatus_t status = ONEWIRE_NOK;
	DS1822xStatus_t retVal = DS1822x_NOK;
	uint8_t data[3] = {0};

	/* Limit to the high temperature limit */
	if(highLimTemp > DS1822x_TMP_HIGH_LIMIT)
	{
		highLimTemp = DS1822x_TMP_HIGH_LIMIT;
	}

	/* Limit to the low temperature limit */
	if(highLimTemp < DS1822x_TMP_LOW_LIMIT)
	{
		highLimTemp = DS1822x_TMP_LOW_LIMIT;
	}

	/* Set the proper values to TH and TL registers */
	data[0] = (uint8_t)highLimTemp;
	//if(highLimTemp < 0)
	//{
	//	//Bit 8 is the sign, change it to negative
	//	data[0] |= 0x80;
	//}
	data[1] = (uint8_t)lowLimTemp;
	//if(lowLimTemp < 0)
	//{
	//	//Bit 8 is the sign, change it to negative
	//	data[1] |= 0x80;
	//}
	data[2] = (uint8_t)sensorResolution;

	/* Select DS1822x */
	status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
	if(status == ONEWIRE_OK)
	{
		/* Write data to scratchpad */
		lib_DS1822x_WriteScratchpad(ds1822xStruct, &data[0]);

		/* Select DS1822x again */
		status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
		if(status == ONEWIRE_OK)
		{
			/* Copy scratchpad to EEPROM */
			lib_DS1822x_CopyScratchpad(ds1822xStruct, device);

			/* Update parameters */
			ds1822xStruct->tempLimitHigh = highLimTemp;
			ds1822xStruct->tempLimitLow = lowLimTemp;
			ds1822xStruct->resolution = sensorResolution;
		}
	}

	return retVal;
}

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/

DS1822xStatus_t lib_DS1822P_Init(DS1822x_t* ds1822xStruct, OneWire_t* oneWireStruct, uint8_t* romAddress, GPIO_TypeDef* gpioStrongPullUp, uint16_t gpioPinStrongPullUp)
{
	return lib_DS1822x_Init(ds1822xStruct, oneWireStruct, romAddress, gpioStrongPullUp, gpioPinStrongPullUp, DEVICE_DS1822P);
}

DS1822xStatus_t lib_DS1822_Init(DS1822x_t* ds1822xStruct, OneWire_t* oneWireStruct, uint8_t* romAddress)
{
	return lib_DS1822x_Init(ds1822xStruct, oneWireStruct, romAddress, NULL, 0, DEVICE_DS1822);
}

DS1822xStatus_t lib_DS1822P_ReadTemperature(DS1822x_t* ds1822xStruct)
{
	return lib_DS1822x_ReadTemperature(ds1822xStruct, DEVICE_DS1822P);
}

DS1822xStatus_t lib_DS1822_ReadTemperature(DS1822x_t* ds1822xStruct)
{
	return lib_DS1822x_ReadTemperature(ds1822xStruct, DEVICE_DS1822);
}

DS1822xStatus_t lib_DS1822x_GetConfig(DS1822x_t* ds1822xStruct)
{
	OneWireStatus_t status = ONEWIRE_NOK;
	DS1822xStatus_t retVal = DS1822x_NOK;

	/* Select DS1822x */
	status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
	if(status == ONEWIRE_OK)
	{
		/* Recall EEPROM data to scratchpad command */
		retVal = lib_DS1822x_RecallScratchpad(ds1822xStruct);
		if(retVal == DS1822x_OK)
		{
			/* Select DS1822x again */
			retVal = DS1822x_NOK;
			status = lib_OneWire_MatchROM(&ds1822xStruct->oneWireStruct, &ds1822xStruct->rom_code[0]);
			if(status == ONEWIRE_OK)
			{
				/* Read scratchpad information */
				retVal = lib_DS1822x_ReadScratchpad(ds1822xStruct);
				if(retVal == DS1822x_OK)
				{
					/* Temperature limits */
					ds1822xStruct->tempLimitHigh = (int8_t)DS1822x_scratchpad[DS1822x_SCRATCHPAD_TH];
					ds1822xStruct->tempLimitLow = (int8_t)DS1822x_scratchpad[DS1822x_SCRATCHPAD_TL];

					/* Temperature resolution analysis */
					ds1822xStruct->resolution = ((DS1822x_scratchpad[DS1822x_SCRATCHPAD_CONFIG] & DS1822x_SCRATCHPAD_CONFIG_MASK) >> DS1822x_SCRATCHPAD_CONFIG_RSHIFT) + (uint8_t)DS1822x_TMP_RES_9;
				}
			}
		}
	}

	return retVal;
}

DS1822xStatus_t lib_DS1822P_SetConfig(DS1822x_t* ds1822xStruct, int8_t highLimTemp, int8_t lowLimTemp, DS1822xTmpRes_t sensorResolution)
{
	return lib_DS1822x_SetConfig(ds1822xStruct, highLimTemp, lowLimTemp, sensorResolution, DEVICE_DS1822P);
}

DS1822xStatus_t lib_DS1822_SetConfig(DS1822x_t* ds1822xStruct, int8_t highLimTemp, int8_t lowLimTemp, DS1822xTmpRes_t sensorResolution)
{
	return lib_DS1822x_SetConfig(ds1822xStruct, highLimTemp, lowLimTemp, sensorResolution, DEVICE_DS1822);
}
