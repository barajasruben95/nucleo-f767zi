/**
 ******************************************************************************
 * @file    lib_UART8.h
 * @author  Ruben Barajas
 * @version V1.0
 * @date    11-September-2023
 * @brief   HEADER - UART8 library for NUCLEO - F767ZI
 ******************************************************************************
 */

#ifndef lib_UART8_H_
#define lib_UART8_H_

/**********************/
/***** LIBRARIES ******/
/**********************/
#include "stm32f7xx.h"

/*****************************/
/***** DATA DEFINITIONS ******/
/*****************************/

///Enumeration to determine the base number to be used
typedef enum
{
	lib_UART8_binary = 2,       /*!< Binary numbers */
	lib_UART8_octal = 8,        /*!< Octal numbers */
	lib_UART8_decimal = 10,     /*!< Decimal numbers */
	lib_UART8_hexadecimal = 16  /*!< Hexadecimal numbers */
} lib_UART8_Base;

///Enumeration to define if base symbol will be sent
///
/// number |mode                               | Binary | Octal  | Decimal | Hexadecimal |
/// ------ | --------------------------------- |------- |------- |-------- | ----------- |
///  14    | lib_UART8_baseVisualizationFALSE   | 1110   | 16     | 14      | E           |
///  ^     | lib_UART8_baseVisualizationTRUE    | 1110b  | 16o    | 14d     | Eh          |
typedef enum
{
	lib_UART8_baseVisualizationFALSE, /*!< Base visualization is not sent */
	lib_UART8_baseVisualizationTRUE /*!< Send base visualization */
} lib_UART8_BaseVisualization;

/**********************/
/***** FUNCTIONS ******/
/**********************/

/**
 * @brief    UART8 initialization (UART + GPIO)
 *
 * UART8 is configured as follows:
 * - Word Length = 8 Bits (8 data bit + no parity bit)
 * - Stop Bit    = One Stop bit
 * - Parity      = No parity
 * - BaudRate    = 115200
 * - Hardware flow control disabled (RTS and CTS signals)
 * @param    None
 * @retval   None
 * @note     No previous requirements
 * @note     UART specification PINS and Alternative Functions
 *
 *  Instance | Function | Pin  | AF mode |
 * --------- |--------- |----- |-------- |
 *  UART 8   | TX       | PE1  | AF8     |
 *  ^        | RX       | PE0  | AF8     |
 */
void lib_UART8_Init(void);

/**
 * @brief    Single byte write
 * @param    data Byte to be written through UART8 TX
 * @retval   None
 * @note     Previous requirement: initialize UART8, refer to @ref lib_UART8_Init
 */
void lib_UART8_Write(uint8_t data);

/**
 * @brief    Multiple bytes write
 * @param    *data Data to be written through UART8 TX
 * @retval   None
 * @note     Previous requirement: initialize UART8, refer to @ref lib_UART8_Init
 */
void lib_UART8_WriteString(uint8_t *data);

/**
 * @brief    Write number character by character
 * @param    number Number to write
 * @param    base Desired base to send the number (Ej. 14 can be: E hexadecimal, 14 decimal, 16 octal or 1110 binary)
 * @param    baseVisualization Send the number base; h hexadecimal, d decimal, o octal or b binary
 * @retval   None
 * @note     Previous requirement: initialize UART8, refer to @ref lib_UART8_Init
 */
void lib_UART8_WriteNumber(uint32_t number, lib_UART8_Base base, lib_UART8_BaseVisualization baseVisualization);

/**
 * @brief    Single float number write
 * @param    number Float number to be written through UART8 TX
 * @param    decimalDigits Amount of digits after dot to send
 * @retval   None
 * @note     Previous requirement: initialize UART8, refer to @ref lib_UART8_Init
 */
void lib_UART8_WriteFloatingNumber(float number, uint8_t decimalDigits);

/**
 * @brief    Single byte read
 * @param    None
 * @retval   Data read from UART8 RX
 * @note     Previous requirement: initialize UART8, refer to @ref lib_UART8_Init
 */
uint8_t lib_UART8_Read(void);

#endif /* lib_UART8_H_ */
