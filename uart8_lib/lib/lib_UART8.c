/**
 ******************************************************************************
 * @file    lib_UART8.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    11-September-2023
 * @brief   UART library for NUCLEO - F767ZI
 ******************************************************************************
 */

/**********************/
/***** LIBRARIES ******/
/**********************/
#include "lib_UART8.h"

/*****************************/
/***** GLOBAL VARIABLES ******/
/*****************************/
UART_HandleTypeDef lib_UART8_InitStruct;

/*****************************/
/***** GLOBAL FUNCTIONS ******/
/*****************************/
extern void lib_UART8_Error_Handler(void);

void lib_UART8_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructTx;
	GPIO_InitTypeDef GPIO_InitStructRx;

	/* Enable clocks first */
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_UART8_CLK_ENABLE();

	/* Configure TX GPIO */
	GPIO_InitStructTx.Pin = GPIO_PIN_1;
	GPIO_InitStructTx.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructTx.Pull = GPIO_NOPULL;
	GPIO_InitStructTx.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructTx.Alternate = GPIO_AF8_UART8;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStructTx);

	/* Configure RX GPIO */
	GPIO_InitStructRx.Pin = GPIO_PIN_0;
	GPIO_InitStructRx.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructRx.Pull = GPIO_NOPULL;
	GPIO_InitStructRx.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructRx.Alternate = GPIO_AF8_UART8;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStructRx);

	/* UART configuration */
	lib_UART8_InitStruct.Instance        = UART8;
	lib_UART8_InitStruct.Init.BaudRate   = 115200;
	lib_UART8_InitStruct.Init.WordLength = UART_WORDLENGTH_8B;
	lib_UART8_InitStruct.Init.StopBits   = UART_STOPBITS_1;
	lib_UART8_InitStruct.Init.Parity     = UART_PARITY_NONE;
	lib_UART8_InitStruct.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	lib_UART8_InitStruct.Init.Mode       = UART_MODE_TX_RX;
	lib_UART8_InitStruct.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&lib_UART8_InitStruct) != HAL_OK)
	{
		/* Initialization Error */
		lib_UART8_Error_Handler();
	}
}

void lib_UART8_Write(uint8_t data)
{
	/* Transmit data */
	if(HAL_UART_Transmit(&lib_UART8_InitStruct, &data, (uint16_t)sizeof(data), (uint32_t)100) != HAL_OK)
	{
		/* Handle error */
		lib_UART8_Error_Handler();
	}
}

void lib_UART8_WriteString(uint8_t *data)
{
	while (*data)
	{
		lib_UART8_Write(*data++);
	}
}

void lib_UART8_WriteNumber(uint32_t number, lib_UART8_Base base, lib_UART8_BaseVisualization baseVisualization)
{
	uint8_t localNumber = 0;
	int8_t i = 0;

	/* Set base format */
	if(base == lib_UART8_hexadecimal)
	{
		localNumber = 8;
	}
	else if(base == lib_UART8_decimal)
	{
		localNumber = 10;
	}
	else if(base == lib_UART8_octal)
	{
		localNumber = 11;
	}
	else if(base == lib_UART8_binary)
	{
		localNumber = 32;
	}
	else
	{
		/* Hexadecimal is by default */
		localNumber = 8;
	}

	/* Digits to be send */
	uint8_t digits[localNumber]; /* Array with max dimension to hold the number */
	while(number)
	{
		localNumber = number % base;

		/* Hexadecimal case */
		if(localNumber > 9)
		{
			if(localNumber == 10)
				digits[i]= 'A';
			else if(localNumber == 11)
				digits[i]= 'B';
			else if(localNumber == 12)
				digits[i]= 'C';
			else if(localNumber == 13)
				digits[i]= 'D';
			else if(localNumber == 14)
				digits[i]= 'E';
			else if(localNumber == 15)
				digits[i]= 'F';
			else
				digits[i]= '@'; /* Send at if fatal error is detected */
		}
		else
			digits[i]= localNumber + '0';

		number = (int32_t)(number / base);
		i++;
	}

	/* Send number through UART8 TX */
	for(i--; i > -1; i--)
	{
		lib_UART8_Write(digits[i]);
	}

	/* Check if base digit is required */
	if(baseVisualization == lib_UART8_baseVisualizationTRUE)
	{
		if(base == lib_UART8_hexadecimal)
		{
			lib_UART8_Write('h');
		}
		else if(base == lib_UART8_decimal)
		{
			lib_UART8_Write('d');
		}
		else if(base == lib_UART8_octal)
		{
			lib_UART8_Write('o');
		}
		else if(base == lib_UART8_binary)
		{
			lib_UART8_Write('b');
		}
		else
		{
			/* Misra C compliance*/
			lib_UART8_Write('@');  /* Send at if fatal error is detected */
		}
	}
}

void lib_UART8_WriteFloatingNumber(float number, uint8_t decimalDigits)
{
	/* In case of negative numbers, print minus symbol */
	if (number < 0.0)
	{
		lib_UART8_Write('-');
		number *= -1;
	}

	/* Rounding up number (if number = 1.9999, it'll be rounded to 2.0) */
	float rounding = 0.5;
	for(uint8_t i = 0; i < decimalDigits; ++i)
	{
		rounding /= 10.0;
	}
	number += rounding;

	/* Extract positive part and send through UART TX */
	uint32_t integerPart = (uint32_t)number;
	uint8_t data[10];
	int8_t i = 0;
	if(integerPart == 0)
	{
		lib_UART8_Write('0');
	}
	else
	{
		while(integerPart != 0)
		{
			data[i] = (integerPart % 10);
			integerPart = (integerPart - data[i]) / 10;
			i++;
		}
	}
	for(i--; i > -1; i--)
	{
		lib_UART8_Write(data[i] + '0');
	}

	/* Print decimal dot if needed */
	if (decimalDigits > 0)
	{
		lib_UART8_Write('.');
	}

	/* Extract decimal digits and send them through UART8 TX */
	integerPart = (uint32_t)number;
	float remainder = number - (float)integerPart;
	while (decimalDigits-- > 0)
	{
		remainder *= 10.0;
		int32_t toPrint = (int32_t)(remainder);
		lib_UART8_Write(toPrint + '0');
		remainder -= toPrint;
	}
}

uint8_t lib_UART8_Read(void)
{
	uint8_t dataReceived = 0;

	/* Receive data - wait 10 seconds only or go to Handle Error */
	if(HAL_UART_Receive(&lib_UART8_InitStruct, &dataReceived, (uint16_t)sizeof(dataReceived), (uint32_t)10000) != HAL_OK)
	{
		/* Handle error */
		lib_UART8_Error_Handler();
	}

	return dataReceived;
}
