/**
 ******************************************************************************
 * @file    main.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    14-February-2024
 * @brief   Default main function.
 ******************************************************************************
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "stm32f7xx.h"
#include "stm32f7xx_nucleo_144.h"
#include "lib_USART3.h"

/*
 *********************
 ***** FUNCTIONS *****
 *********************
 */
void lib_USART3_Error_Handler(void);
static void CPU_CACHE_Enable(void);
static void SystemClock_Config(void);
static void Error_Handler(void);

/**
 * @brief  Main
 * @param  None
 * @retval None
 */
int main(void)
{
	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	/* STM32F7xx HAL library initialization:
     - Configure the Flash prefetch
     - Systick timer is configured by default as source of time base, but user
       can eventually implement his proper time base source (a general purpose
       timer for example or other time source), keeping in mind that Time base
       duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
       handled in milliseconds basis.
     - Set NVIC Group Priority to 4
     - Low Level Initialization
	 */
	HAL_Init();

	/* Configure the system clock to 216 MHz */
	SystemClock_Config();

	/* Initialize USART3 */
	lib_USART3_Init();

	/* STRING */
	uint8_t data2send[] = "Hi STM32\n\r";
	lib_USART3_Write(&data2send[0], (uint16_t)sizeof(data2send));

	/* NUMBERS */
	uint8_t number2send = 0xE;
	uint32_t largeNumber2send = 3133445673; /* 0xBAC49629 */
	uint8_t character_crlf[] = "\n\r";

	/* Write number */
	lib_USART3_WriteNumber(number2send, lib_USART3_binary);      /* 1110 */
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));
	lib_USART3_WriteNumber(number2send, lib_USART3_octal);       /* 16 */
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));
	lib_USART3_WriteNumber(number2send, lib_USART3_decimal);     /* 14 */
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));
	lib_USART3_WriteNumber(number2send, lib_USART3_hexadecimal); /* E */
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));
	lib_USART3_WriteNumber(largeNumber2send, lib_USART3_hexadecimal);    /* 0xBAC49629 */
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));

	/* Write floating number */
	float float2send = 16.77821;
	float negativeFloat2send = -22.14;
	lib_USART3_WriteFloatingNumber(float2send, 3);
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));    /* 16.778 */
	lib_USART3_WriteFloatingNumber(float2send, 5);
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));    /* 16.77821 */
	lib_USART3_WriteFloatingNumber(float2send, 8);
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));    /* 16.77821xxx */
	lib_USART3_WriteFloatingNumber(negativeFloat2send, 5);
	lib_USART3_Write(&character_crlf[0], (uint16_t)sizeof(character_crlf));    /* -22.14xxx */

	/* End */
	while(1);
}

/**
 * @brief  CPU L1-Cache enable
 * @param  None
 * @retval None
 */
static void CPU_CACHE_Enable(void)
{
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

/**
 * @brief System Clock Configuration
 *
 * The system Clock is configured as follow:
 *   System Clock source            = PLL (HSE)
 *   SYSCLK(Hz)                     = 216000000
 *   HCLK(Hz)                       = 216000000
 *   AHB Prescaler                  = 1
 *   APB1 Prescaler                 = 4
 *   APB2 Prescaler                 = 2
 *   HSE Frequency(Hz)              = 8000000
 *   PLL_M                          = 8
 *   PLL_N                          = 432
 *   PLL_P                          = 2
 *   PLL_Q                          = 9
 *   PLL_R                          = 7
 *   VDD(V)                         = 3.3
 *   Main regulator output voltage  = Scale1 mode
 *   Flash Latency(WS)              = 7
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
	RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 432;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	RCC_OscInitStruct.PLL.PLLR = 7;
	if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		/* Handle error */
		Error_Handler();
	}

	/* Activate the OverDrive to reach the 216 MHz Frequency */
	if(HAL_PWREx_EnableOverDrive() != HAL_OK)
	{
		/* Handle error */
		while(1);
	}

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
	{
		/* Handle error */
		while(1);
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void)
{
	/* Handle error */
	while(1);
}

/**
 * @brief  This function is executed in case of USART3 error occurrence.
 * @param  None
 * @retval None
 */
void lib_USART3_Error_Handler(void)
{
	/* Handle USART3 errors */
	while(1);
}
