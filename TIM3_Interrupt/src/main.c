/**
 ******************************************************************************
 * @file    main.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    22-September-2023
 * @brief   Default main function.
 ******************************************************************************
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "stm32f7xx.h"
#include "stm32f7xx_nucleo_144.h"

/*
 ***********************
 ***** DEFINITIONS *****
 ***********************
 */
#define TIMx                 TIM3
#define TIMx_CLK_ENABLE()    __HAL_RCC_TIM3_CLK_ENABLE()
#define TIMx_IRQn            TIM3_IRQn
#define TIMx_IRQHandler      TIM3_IRQHandler

/*
 ****************************
 ***** STATIC FUNCTIONS *****
 ****************************
 */
static void CPU_CACHE_Enable(void);
static void SystemClock_Config(void);
static void Error_Handler(void);

/*
 ****************************
 ***** GLOBAL VARIABLES *****
 ****************************
 */
TIM_HandleTypeDef timHandle; /* TIM handle declaration */
__IO uint32_t uwPrescalerValue = 0; /* Preescaler declaration */

/**
 * @brief  Main
 * @param  None
 * @retval None
 */
int main(void)
{
	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	/* STM32F7xx HAL library initialization:
     - Configure the Flash prefetch
     - Systick timer is configured by default as source of time base, but user
       can eventually implement his proper time base source (a general purpose
       timer for example or other time source), keeping in mind that Time base
       duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
       handled in milliseconds basis.
     - Set NVIC Group Priority to 4
     - Low Level Initialization
	 */
	HAL_Init();

	/* Configure the system clock to 216 MHz */
	SystemClock_Config();

	/* Configure LED BLUE */
	BSP_LED_Init(LED_BLUE);

	/* 1- Configure the TIM peripheral */
	/* -----------------------------------------------------------------------
        In this example TIM3 input clock (TIM3CLK)  is set to APB1 clock (PCLK1) x2,
        since APB1 prescaler is equal to 4.
          TIM3CLK = PCLK1*2
          PCLK1 = HCLK/4
          => TIM3CLK = HCLK/2 = SystemCoreClock/2
        To get TIM3 counter clock at 10 KHz, the Prescaler is computed as follows:
        Prescaler = (TIM3CLK / TIM3 counter clock) - 1
        Prescaler = ((SystemCoreClock/2) /10 KHz) - 1

        Note:
         SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f7xx.c file.
         Each time the core clock (HCLK) changes, user had to update SystemCoreClock
         variable value. Otherwise, any configuration based on this variable will be incorrect.
         This variable is updated in three ways:
          1) by calling CMSIS function SystemCoreClockUpdate()
          2) by calling HAL API function HAL_RCC_GetSysClockFreq()
          3) each time HAL_RCC_ClockConfig() is called to configure the system clock frequency
      ----------------------------------------------------------------------- */

	/* Compute the prescaler value to have TIMx counter clock equal to 10000 Hz */
	uwPrescalerValue = (uint32_t)((SystemCoreClock/ 2) / 10000) - 1;

	/* Set TIMx instance */
	timHandle.Instance = TIMx;

	/* Initialize TIMx peripheral as follows:
           + Period = 10000 - 1
           + Prescaler = ((SystemCoreClock / 2)/10000) - 1
           + ClockDivision = 0
           + Counter direction = Up
	 */
	timHandle.Init.Period            = 10000 - 1;
	timHandle.Init.Prescaler         = uwPrescalerValue;
	timHandle.Init.ClockDivision     = 0;
	timHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
	timHandle.Init.RepetitionCounter = 0;
	timHandle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&timHandle) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}

	/* 2- Start the TIM Base generation in interrupt mode */
	/* Start Channel1 */
	if (HAL_TIM_Base_Start_IT(&timHandle) != HAL_OK)
	{
		/* Starting Error */
		Error_Handler();
	}

	while(1);
}

/**
 * @brief  CPU L1-Cache enable
 * @param  None
 * @retval None
 */
static void CPU_CACHE_Enable(void)
{
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

/**
 * @brief System Clock Configuration
 *
 * The system Clock is configured as follow:
 *   System Clock source            = PLL (HSE)
 *   SYSCLK(Hz)                     = 216000000
 *   HCLK(Hz)                       = 216000000
 *   AHB Prescaler                  = 1
 *   APB1 Prescaler                 = 4
 *   APB2 Prescaler                 = 2
 *   HSE Frequency(Hz)              = 8000000
 *   PLL_M                          = 8
 *   PLL_N                          = 432
 *   PLL_P                          = 2
 *   PLL_Q                          = 9
 *   PLL_R                          = 7
 *   VDD(V)                         = 3.3
 *   Main regulator output voltage  = Scale1 mode
 *   Flash Latency(WS)              = 7
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
	RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 432;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	RCC_OscInitStruct.PLL.PLLR = 7;
	if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		/* Handle error */
		Error_Handler();
	}

	/* Activate the OverDrive to reach the 216 MHz Frequency */
	if(HAL_PWREx_EnableOverDrive() != HAL_OK)
	{
		/* Handle error */
		while(1);
	}

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
	{
		/* Handle error */
		while(1);
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void)
{
	/* Handle error */
	while(1);
}

/**
 * @brief  This function handles TIM interrupt request.
 * @param  None
 * @retval None
 */
void TIMx_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&timHandle);
}

/**
 * @brief  Period elapsed callback in non blocking mode
 * @param  htim : TIM handle
 * @retval None
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	BSP_LED_Toggle(LED_BLUE);
}

/**
 * @brief TIM MSP Initialization
 *        This function configures the hardware resources used in this example:
 *           - Peripheral's clock enable
 * @param htim: TIM handle pointer
 * @retval None
 */
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
	/*##-1- Enable peripheral clock #################################*/
	/* TIMx Peripheral clock enable */
	TIMx_CLK_ENABLE();

	/*##-2- Configure the NVIC for TIMx ########################################*/
	/* Set the TIMx priority */
	HAL_NVIC_SetPriority(TIMx_IRQn, 3, 0);

	/* Enable the TIMx global Interrupt */
	HAL_NVIC_EnableIRQ(TIMx_IRQn);
}
