/**
 ******************************************************************************
 * @file    main.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    26-March-2021
 * @brief   Default main function.
 ******************************************************************************
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "stm32f7xx.h"
#include "stm32f7xx_nucleo_144.h"
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>

/*
 ***********************
 ***** DEFINITIONS *****
 ***********************
 */
/* DWT control register */
#define DWT_CONTROL (*(volatile uint32_t*)0xE0001000)

/* DWT count register */
#define DWT_CYCCNT (*(volatile uint32_t*)0xE0001004)

/* Debug Exception and Monitor Control Register */
#define DEMCR (*(volatile uint32_t*)0xE000EDFC)

/* LSR and LAR, Software Lock Status Register and Software Lock Access Register */
#define LAR (*(volatile uint32_t*)0xE0001FB0)

/*
 ****************************
 ***** STATIC FUNCTIONS *****
 ****************************
 */
static void CPU_CACHE_Enable(void);
static void SystemClock_Config(void);
static void Task1(void* parameters);
static void Task2(void* parameters);
static void Task3(void* parameters);

/**
 * @brief  Main
 * @param  None
 * @retval None
 */
int main(void)
{
	TaskHandle_t task1_handle;
	TaskHandle_t task2_handle;
	TaskHandle_t task3_handle;
	BaseType_t status;

	/* Enable the CPU Cache */
	CPU_CACHE_Enable();

	/* STM32F7xx HAL library initialization:
	 - Configure the Flash prefetch
	 - Systick timer is configured by default as source of time base, but user
	   can eventually implement his proper time base source (a general purpose
	   timer for example or other time source), keeping in mind that Time base
	   duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and
	   handled in milliseconds basis.
	 - Set NVIC Group Priority to 4
	 - Low Level Initialization
	 */
	HAL_Init();

	/* Configure the system clock to 216 MHz */
	SystemClock_Config();

	/* Initialize LED */
	BSP_LED_Init(LED_RED);
	BSP_LED_Init(LED_BLUE);
	BSP_LED_Init(LED_GREEN);

	/* Enable the use DWT */
	DEMCR |= 0x01000000;
	/* Added unlock access to DWT (ITM, etc.) registers */
	LAR = 0xC5ACCE55;
	/* Reset cycle counter */
	DWT_CYCCNT = 0;
	/* Enable cycle counter */
	DWT_CONTROL |= 1;

	/* ENABLE SEGGER */
	SEGGER_UART_init(100000);
	SEGGER_SYSVIEW_Conf();
	//SEGGER_SYSVIEW_Start();

	/* TASKS */
	status = xTaskCreate(Task1, "Task-1", 200, "Hello world from Task-1", 2, &task1_handle);
	configASSERT(status == pdPASS);

	status = xTaskCreate(Task2, "Task-2", 200, "Hello world from Task-2", 2, &task2_handle);
	configASSERT(status == pdPASS);

	status = xTaskCreate(Task3, "Task-3", 200, "Hello world from Task-3", 2, &task3_handle);
	configASSERT(status == pdPASS);

	/* Start the freeRTOS scheduler */
	vTaskStartScheduler();

	/* If the control comes here, then the launch of the scheduler has failed due to
	insufficient memory in heap */

	/* Infinite loop */
	while (1);
}

/**
 * @brief  CPU L1-Cache enable
 * @param  None
 * @retval None
 */
static void CPU_CACHE_Enable(void)
{
	/* Enable I-Cache */
	SCB_EnableICache();

	/* Enable D-Cache */
	SCB_EnableDCache();
}

/**
 * @brief System Clock Configuration
 *
 * The system Clock is configured as follow:
 *   System Clock source            = PLL (HSE)
 *   SYSCLK(Hz)                     = 216000000
 *   HCLK(Hz)                       = 216000000
 *   AHB Prescaler                  = 1
 *   APB1 Prescaler                 = 4
 *   APB2 Prescaler                 = 2
 *   HSE Frequency(Hz)              = 8000000
 *   PLL_M                          = 8
 *   PLL_N                          = 432
 *   PLL_P                          = 2
 *   PLL_Q                          = 9
 *   PLL_R                          = 7
 *   VDD(V)                         = 3.3
 *   Main regulator output voltage  = Scale1 mode
 *   Flash Latency(WS)              = 7
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
	RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 432;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	RCC_OscInitStruct.PLL.PLLR = 7;
	if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		/* Handle error */
		while(1);
	}

	/* Activate the OverDrive to reach the 216 MHz Frequency */
	if(HAL_PWREx_EnableOverDrive() != HAL_OK)
	{
		/* Handle error */
		while(1);
	}

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
	{
		/* Handle error */
		while(1);
	}
}

/**
 * @brief  Initialize the MSP callback.
 * @retval None
 */
void HAL_MspInit(void)
{
	/* If the application only uses CMSIS libraries for interrupt
	 * configuration then the correct setting can be achieved on all Cortex-M
	 * devices by calling NVIC_SetPriorityGrouping( 0 ); before starting the
	 * scheduler.  */
	//NVIC_SetPriorityGrouping( 0 );
	vInitPrioGroupValue();
}

static void Task1(void* parameters)
{
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = 2000;
	char msg[50];

	// Initialize the xLastWakeTime variable with the current time.
	xLastWakeTime = xTaskGetTickCount();

	/* Format string */
	//printf("%s\n", (char*) parameters")
	snprintf(msg, sizeof(msg), "%s\n", (char*) parameters);

	/* Print information through Segger */
	SEGGER_SYSVIEW_PrintfTarget(msg);

	while(1)
	{
		/* Blink LED */
		BSP_LED_Toggle(LED_RED);

		// Wait for the next cycle.
		xTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS( xFrequency ) );
	}

	/* In case of break out the above loop, delete the task */
	vTaskDelete(NULL);
}

static void Task2(void* parameters)
{
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = 1000;
	char msg[50];

	// Initialize the xLastWakeTime variable with the current time.
	xLastWakeTime = xTaskGetTickCount();

	/* Format string */
	//printf("%s\n", (char*) parameters")
	snprintf(msg, sizeof(msg), "%s\n", (char*) parameters);

	/* Print information through Segger */
	SEGGER_SYSVIEW_PrintfTarget(msg);

	while(1)
	{
		/* Blink LED */
		BSP_LED_Toggle(LED_BLUE);

		// Wait for the next cycle.
		xTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS( xFrequency ) );
	}

	/* In case of break out the above loop, delete the task */
	vTaskDelete(NULL);
}

static void Task3(void* parameters)
{
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = 500;
	char msg[50];

	// Initialize the xLastWakeTime variable with the current time.
	xLastWakeTime = xTaskGetTickCount();

	/* Format string */
	//printf("%s\n", (char*) parameters")
	snprintf(msg, sizeof(msg), "%s\n", (char*) parameters);

	/* Print information through Segger */
	SEGGER_SYSVIEW_PrintfTarget(msg);

	while(1)
	{
		/* Blink LED */
		BSP_LED_Toggle(LED_GREEN);

		// Wait for the next cycle.
		xTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS( xFrequency ) );
	}

	/* In case of break out the above loop, delete the task */
	vTaskDelete(NULL);
}

