/**
 * @file    lib_LCD_I2C.h
 * @date    15-February-2024
 * @brief   HEADER LCD I2C library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "stm32f7xx.h"

#ifndef LIB_LCD_I2C_H_
#define LIB_LCD_I2C_H_

#define LCD_I2C_ACTIVATE_RS         0b00000001
#define LCD_I2C_DEACTIVATE_RS       0b00000000
#define LCD_I2C_ACTIVATE_E          0b00000100
#define LCD_I2C_DEACTIVATE_E        0b11111001

#define LCD_I2C_COMMAND_INITIALIZATION 0b00110000
#define LCD_I2C_COMMAND_CLEAR          0b00000001
#define LCD_I2C_COMMAND_LINE1          0b10000000
#define LCD_I2C_COMMAND_LINE2          0b11000000
#define LCD_I2C_COMMAND_LCD_OFF        0b00001000
#define LCD_I2C_COMMAND_CURSOR_OFF     0b00001100
#define LCD_I2C_COMMAND_CURSOR_ON      0b00001110

#define LCD_I2C_DELAY_COMMAND             2U     /* 2ms DELAY WHEN SENDING COMMANDS */
#define LCD_I2C_DELAY_DATA                50U    /* 50us DELAY WHEN SENDING DATA */
#define LCD_I2C_DELAY_INITIALIZATION1     20U    /* 20ms DELAY WHEN FIRST DELAY INITIALIZATION */
#define LCD_I2C_DELAY_INITIALIZATION2     5U     /* 5ms DELAY WHEN SECOND DELAY INITIALIZATION */
#define LCD_I2C_DELAY_INITIALIZATION3     200U   /* 200us DELAY WHEN THIRD DELAY INITIALIZATION */

/*****************************/
/***** DATA DEFINITIONS ******/
/*****************************/
///Enumeration to determine the information type send to the LCD
typedef enum
{
	lib_LCD_I2C_InfoType_command,    /*!< Information sent is a command */
	lib_LCD_I2C_InfoType_data        /*!< Information sent is data */
} lib_LCD_I2C_InfoType;

/*
 ********************************
 ***** FUNCTIONS PROTOTYPES *****
 ********************************
 */

extern void I2C_Error_Handler(void);

/*
 * Inicializar pantalla LCD encendida en formato de 4 bits y con el cursor encendido
 * 		Requisitos previos->	I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_Init(uint8_t address);

/*
 * Borra toda la pantalla, memoria DDRAM y pone el cursor al comienzo de la Line 1
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_Clear(uint8_t address);

/*
 * Cursor al comienzo de la Line 1
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_Line1(uint8_t address);

/*
 * Cursor al comienzo de la Line 2
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_Line2 (uint8_t address);

/*
 * Desplazar el cursor cierta cantidad de posiciones en la linea 1 desde el comienzo de la linea
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								position:	numero de posiciones a desplazar el cursor
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_PositionLine1(uint8_t address, uint8_t position);

/*
 * Desplazar el cursor cierta cantidad de posiciones en la linea 2 desde el comienzo de la linea
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								position:	numero de posiciones a desplazar el cursor
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_PositionLine2(uint8_t address, uint8_t position);

/*
 * Apagar la pantalla LCD
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_Off(uint8_t address);

/*
 * Pantalla encendida y cursor encendido
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_CursorOn(uint8_t address);

/*
 * Pantalla encendida y cursor apagado
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_CursorOff(uint8_t address);

/*
 * Escribir un data puro (sin conocer si es ASCII o numero). Se traducira en el valor de la tabla de
 * codigos CGROM contenida en la LCD
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								data:		data a escribir
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_Write(uint8_t address, uint8_t data);

/*
 * Escribir una cadena de datos que se visualizaran en la pantalla
 * 		Requisitos previos->	lib_LCD_I2C_Init();
 * 		Entrada->				*data:	direccion del primer data que sera escrito
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_WriteString(uint8_t address, uint8_t *data);

/*
 * Escribir un numero en formato decimal
 *		Requisitos previos->	lib_LCD_I2C_Init();
 *								Activar la unidad de punto flotante
 * 		Entrada->				address:		direccion I2C del dispositivo a comunicarse
 * 								number:			numero a ser escrito. El maximo numero permitido es de 32bits
 * 								decimalDigits:	cantidad de digitos decimales del numero que desean visualizarse en la pantalla
 * 		Salida->				Ninguno
 */
void lib_LCD_I2C_WriteFloatingNumber(uint8_t address, float number, uint8_t decimalDigits);

#endif /* LIB_LCD_I2C_H_ */
