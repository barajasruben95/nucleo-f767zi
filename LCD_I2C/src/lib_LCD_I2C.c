/**
 * @file    lib_LCD_I2C.c
 * @date    15-February-2024
 * @brief   LCD I2C library for NUCLEO - F767ZI
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 * @license MIT
 *
\verbatim
   ----------------------------------------------------------------------
    Copyright (c) 2023

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software,
    and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
    AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
   ----------------------------------------------------------------------
\endverbatim
 */

/*
 *********************
 ***** LIBRARIES *****
 *********************
 */
#include "lib_LCD_I2C.h"
#include "lib_Delay.h"

/*****************************/
/***** DATA DEFINITIONS ******/
/*****************************/

///Enumeration to determine operation format of the LCD
typedef enum
{
	lib_LCD_I2C_FormatType_8bits,    /*!< LCD is working in 8 bits format */
	lib_LCD_I2C_FormatType_4bits     /*!< LCD is working in 4 bits format */
} lib_LCD_I2C_FormatType;

/*
 ****************************
 ***** GLOBAL VARIABLES *****
 ****************************
 */

/** I2C object */
I2C_HandleTypeDef LCD_I2C_hi2c1;

/** Stores the last data sent through I2C */
uint8_t LCD_I2C_currentStatus;

/** LCD_I2C_bitsFormat = lib_LCD_I2C_FormatType_8bits -> LCD in 8 bits mode
 *  LCD_I2C_bitsFormat = lib_LCD_I2C_FormatType_4bits -> LCD in 4 bits mode */
lib_LCD_I2C_FormatType LCD_I2C_bitsFormat;

/** Allow only one initialization */
uint8_t LCD_I2C_initialize = 0;

/*
 ***************************
 ***** LOCAL FUNCTIONS *****
 ***************************
 */

/**
 * @brief I2C1 and GPIO initialization function
 * @param None
 * @retval None
 */
static void lib_LCD_I2C_Config(void)
{
	if(LCD_I2C_initialize == 0)
	{
		GPIO_InitTypeDef GPIO_InitStruct = {0};
		RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

		LCD_I2C_hi2c1.Instance = I2C1;
		LCD_I2C_hi2c1.Init.Timing = 0x20404768;
		LCD_I2C_hi2c1.Init.OwnAddress1 = 0;
		LCD_I2C_hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
		LCD_I2C_hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
		LCD_I2C_hi2c1.Init.OwnAddress2 = 0;
		LCD_I2C_hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
		LCD_I2C_hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
		LCD_I2C_hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
		if(HAL_I2C_Init(&LCD_I2C_hi2c1) != HAL_OK)
		{
			I2C_Error_Handler();
		}

		if(LCD_I2C_hi2c1.Instance==I2C1)
		{
			/** Initializes the peripherals clock */
			PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
			PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
			if(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
			{
				I2C_Error_Handler();
			}

			__HAL_RCC_GPIOB_CLK_ENABLE();
			/** I2C1 GPIO Configuration
            PB6     ------> I2C1_SCL
            PB9     ------> I2C1_SDA
			 */
			GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_9;
			GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
			HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

			/* Peripheral clock enable */
			__HAL_RCC_I2C1_CLK_ENABLE();
		}

		/** Configure Analog filter */
		if(HAL_I2CEx_ConfigAnalogFilter(&LCD_I2C_hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
		{
			I2C_Error_Handler();
		}

		/** Configure Digital filter */
		if(HAL_I2CEx_ConfigDigitalFilter(&LCD_I2C_hi2c1, 0) != HAL_OK)
		{
			I2C_Error_Handler();
		}

		/* lib_I2C_I2C initialization done */
		LCD_I2C_initialize = 1;
	}
}

/**
 * @brief Send data through the I2C bus
 *
 * @param    address         I2C device to communicate
 * @param    data            Information to send
 * @param    informationType Data type to send: command or data itself, refer to @ref lib_LCD_I2C_InfoType
 * @retval   None
 * @note     INTERNAL FUNCTION ONLY
 * @note     ELECTRICAL CONEXION BETWEEN LCD AND PCF8574
 *
 *  PIN LCD  | PIN PCF8574 |
 * --------- |------------ |
 *  D4       | P4          |
 *  D5       | P5          |
 *  D6       | P6          |
 *  D7       | P7          |
 *  RS       | P0          |
 *  RW       | P1          |
 *  E        | P2          |
 *  VEE      | P3          |
 */
static void lib_LCD_I2C_Send(uint8_t address, uint8_t data, lib_LCD_I2C_InfoType informationType)
{
	uint8_t bufferWrite[4];
	LCD_I2C_currentStatus = 0b00001000; /* Just backlight on */

	/* Check whether command or data */
	if(informationType == lib_LCD_I2C_InfoType_command)
	{
		/* RS = 0. COMMAND */
		LCD_I2C_currentStatus = LCD_I2C_DEACTIVATE_RS | LCD_I2C_currentStatus;
		bufferWrite[0] = LCD_I2C_currentStatus;
	}
	else /* (informationType == lib_LCD_I2C_InfoType_data) */
	{
		/* RS = 1. DATA */
		LCD_I2C_currentStatus = LCD_I2C_ACTIVATE_RS | LCD_I2C_currentStatus;
		bufferWrite[0] = LCD_I2C_currentStatus;
	}

	/* Rising edge in E pin from the LCD */
	LCD_I2C_currentStatus = LCD_I2C_ACTIVATE_E | LCD_I2C_currentStatus;
	bufferWrite[1] = LCD_I2C_currentStatus;

	/* Send nibble high in case of 4bits type or send the entire byte of information */
	LCD_I2C_currentStatus = LCD_I2C_currentStatus | (data & 0b11110000);
	bufferWrite[2] = LCD_I2C_currentStatus;

	/* Capture data with falling edge in E pin from the LCD */
	LCD_I2C_currentStatus = LCD_I2C_DEACTIVATE_E & LCD_I2C_currentStatus;
	bufferWrite[3] = LCD_I2C_currentStatus;

	/* Send the previous sequence */
	if(HAL_I2C_Master_Transmit(&LCD_I2C_hi2c1, address, bufferWrite, sizeof(bufferWrite), 10000) != HAL_OK)
	{
		I2C_Error_Handler();
	}

	/* Delay will depend whether command or data sent */
	if(informationType == lib_LCD_I2C_InfoType_command)
	{
		lib_Delay_MS(LCD_I2C_DELAY_COMMAND); 	/* COMMAND */
	}
	else /* (informationType == lib_LCD_I2C_InfoType_data) */
	{
		lib_Delay_US(LCD_I2C_DELAY_DATA);	/* DATA */
	}

	/* Check if LCD format is 4 bits or 8 bits */
	if(LCD_I2C_bitsFormat == lib_LCD_I2C_FormatType_4bits)
	{
		/* 4 BITS FORMAT, SEND MISSING INFORMATION */

		/* Keep status of VEE, E, RW and RS pins from the LCD */
		LCD_I2C_currentStatus = LCD_I2C_currentStatus & 0b00001111;

		/* Rising edge in E pin from the LCD */
		LCD_I2C_currentStatus = LCD_I2C_ACTIVATE_E | (LCD_I2C_currentStatus & 0b00001001);
		bufferWrite[0] = LCD_I2C_currentStatus;

		/* Send nibble low */
		LCD_I2C_currentStatus = LCD_I2C_currentStatus | (data << 4);
		bufferWrite[1] = LCD_I2C_currentStatus;

		/* Capture data with falling edge in E pin from the LCD */
		LCD_I2C_currentStatus = LCD_I2C_DEACTIVATE_E & LCD_I2C_currentStatus;
		bufferWrite[2] = LCD_I2C_currentStatus;

		/* Send the previous sequence */
		if(HAL_I2C_Master_Transmit(&LCD_I2C_hi2c1, address, bufferWrite, sizeof(bufferWrite) - 1, 10000) != HAL_OK)
		{
			I2C_Error_Handler();
		}

		/* Delay will depend whether command or data sent */
		if(informationType == lib_LCD_I2C_InfoType_command)
		{
			lib_Delay_MS(LCD_I2C_DELAY_COMMAND); 	/* COMMAND */
		}
		else /* (informationType == lib_LCD_I2C_InfoType_data) */
		{
			lib_Delay_US(LCD_I2C_DELAY_DATA);	/* DATA */
		}
	}
}

/*
 ****************************
 ***** GLOBAL FUNCTIONS *****
 ****************************
 */

void lib_LCD_I2C_Init(uint8_t address)
{
	/* Initialize delay library */
	lib_Delay_Init();

	/* Initialize I2C and GPIO */
	lib_LCD_I2C_Config();

	/* By default, 8 bits format is selected */
	LCD_I2C_bitsFormat = lib_LCD_I2C_FormatType_8bits;

	/* LCD initialization. Send command 0b00110000 three times */
	lib_Delay_MS(LCD_I2C_DELAY_INITIALIZATION1);
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_INITIALIZATION, lib_LCD_I2C_InfoType_command);
	lib_Delay_MS(LCD_I2C_DELAY_INITIALIZATION2);
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_INITIALIZATION, lib_LCD_I2C_InfoType_command);
	lib_Delay_US(LCD_I2C_DELAY_INITIALIZATION3);
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_INITIALIZATION, lib_LCD_I2C_InfoType_command);

	/* Setup screen */

	/* LCD in 4 bits format, 2 lines and 5x8 dots per character */
	lib_LCD_I2C_Send(address, 0b00101000, lib_LCD_I2C_InfoType_command);
	LCD_I2C_bitsFormat = lib_LCD_I2C_FormatType_4bits;
	/* Increment cursor to the right when writing a character in the LCD */
	lib_LCD_I2C_Send(address, 0b00000110, lib_LCD_I2C_InfoType_command);
	/* Cursor ON */
	lib_LCD_I2C_CursorOn(address);
	/* LCD clear */
	lib_LCD_I2C_Clear(address);
}

void lib_LCD_I2C_Write(uint8_t address, uint8_t data)
{
	lib_LCD_I2C_Send(address, data, lib_LCD_I2C_InfoType_data);
}

void lib_LCD_I2C_WriteString(uint8_t address, uint8_t *data)
{
	while(*data)
	{
		lib_LCD_I2C_Send(address, *data++, lib_LCD_I2C_InfoType_data);
	}
}

void lib_LCD_I2C_WriteFloatingNumber(uint8_t address, float number, uint8_t decimalDigits)
{
	/* Check if number is negative */
	if(number < 0.0)
	{
		lib_LCD_I2C_Send(address, '-', lib_LCD_I2C_InfoType_data); /* Write minus symbol */
		number *= -1;    /* Change sign for better processing */
	}

	/* Rounding up: if 1.999, then equal to 2.000 */
	float rounding = 0.5;
	for(uint8_t i = 0; i < decimalDigits; ++i)
	{
		rounding /= 10.0;
	}
	number += rounding;

	/* Extract integer part and print */
	uint32_t integerPart = (uint32_t)number;
	uint8_t data[10];
	int8_t i = 0;
	if(integerPart == 0)
	{
		/* Integer part if zero, print it */
		lib_LCD_I2C_Send(address, '0', lib_LCD_I2C_InfoType_data);
	}
	else
	{
		/* Integer part is different than zero, process and print integer part */
		while(integerPart != 0)
		{
			data[i] = (integerPart % 10);
			integerPart = (integerPart - data[i]) / 10;
			i++;
		}
		for(i--; i > -1; i--)
		{
			lib_LCD_I2C_Send(address, data[i] + '0', lib_LCD_I2C_InfoType_data);
		}
	}

	/* Print decimal dot if needed */
	if(decimalDigits > 0)
	{
		lib_LCD_I2C_Send(address, '.', lib_LCD_I2C_InfoType_data);
	}

	/* Process and print decimal part */
	integerPart = (uint32_t)number;
	float remainder = number - (float)integerPart;
	while(decimalDigits-- > 0)
	{
		remainder *= 10.0;
		int32_t toPrint = (int32_t)(remainder);
		lib_LCD_I2C_Send(address, toPrint + '0', lib_LCD_I2C_InfoType_data);
		remainder -= toPrint;
	}
}

void lib_LCD_I2C_Clear(uint8_t address)
{
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_CLEAR, lib_LCD_I2C_InfoType_command);
}

void lib_LCD_I2C_Line1(uint8_t address)
{
	/* Cursor at address 0x00 of LCD DDRAM */
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_LINE1, lib_LCD_I2C_InfoType_command);
}


void lib_LCD_I2C_Line2(uint8_t address)
{
	/* Cursor at address 0x40 of LCD DDRAM */
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_LINE2, lib_LCD_I2C_InfoType_command);
}


void lib_LCD_I2C_PositionLine1(uint8_t address, uint8_t position)
{
	/* Cursor at address 0x00 + offset of LCD DDRAM */
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_LINE1 + position, lib_LCD_I2C_InfoType_command);
}

void lib_LCD_I2C_PositionLine2(uint8_t address, uint8_t position)
{
	/* Cursor at address 0x40 + offset of LCD DDRAM */
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_LINE2 + position, lib_LCD_I2C_InfoType_command);
}

void lib_LCD_I2C_Off(uint8_t address)
{
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_LCD_OFF, lib_LCD_I2C_InfoType_command);
}

void lib_LCD_I2C_CursorOn(uint8_t address)
{
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_CURSOR_ON, lib_LCD_I2C_InfoType_command);
}

void lib_LCD_I2C_CursorOff(uint8_t address)
{
	lib_LCD_I2C_Send(address, LCD_I2C_COMMAND_CURSOR_OFF, lib_LCD_I2C_InfoType_command);
}
