STEPS

1.- Modify the "LinkerScript.ld" file to configure RAM memory as the main memory instead of the flash memory.

2.- Uncomment line "#define VECT_TAB_SRAM" from the file "system_stm32f7xx.c" to relocate the vector table.

/************************* Miscellaneous Configuration ************************/

/*!< Uncomment the following line if you need to relocate your vector Table in
     Internal SRAM. */
#define VECT_TAB_SRAM
#define VECT_TAB_OFFSET  0x00 /*!< Vector Table base offset field. 
                                   This value must be a multiple of 0x200. */
/******************************************************************************/

3.- Use the button to debug the aplication to load the code in RAM.