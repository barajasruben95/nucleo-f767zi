/**
 ******************************************************************************
 * @file    Task_Scheduler.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    21-February-2024
 * @brief   Cooperative scheduler RTOS
 ******************************************************************************
 */

#include "Task_Scheduler.h"

sTaskH hSCH_tasks_G[hSCH_MAX_TASKS];

/*******************************************************************************
 * Function: hSCH_Init
 ******************************************************************************/
void hSCH_Init(void)
{
	uint8_t index;

	for(index = 0; index < hSCH_MAX_TASKS; index++)
	{
		hSCH_tasks_G[index].pTask  = NULL;
		hSCH_tasks_G[index].Delay  = 0;
		hSCH_tasks_G[index].Period = 0;
		hSCH_tasks_G[index].RunMe  = 0;
		hSCH_tasks_G[index].Co_op  = 1;
		hSCH_tasks_G[index].Enable = 0;
	}
}

/*******************************************************************************
 * Function: hSCH_Dispatch_Tasks
 ******************************************************************************/
void hSCH_Dispatch_Tasks(void)
{
	uint8_t index;

	for (index = 0; index < hSCH_MAX_TASKS; index++)
	{
		if ((hSCH_tasks_G[index].Co_op) && (hSCH_tasks_G[index].RunMe > 0))
		{
			if(hSCH_tasks_G[index].pTask != NULL)
			{
				hSCH_tasks_G[index].pTask();
			}
			hSCH_tasks_G[index].RunMe -= 1;
		}

		if (hSCH_tasks_G[index].Period == 0)
		{
			hSCH_tasks_G[index].pTask = NULL;
		}
	}
}  

/*******************************************************************************
 * Function: SCH_Add_Task
 ******************************************************************************/
sTaskH *SCH_Add_Task(TASK_SCH *pFunction, uint32_t delay, uint32_t period, uint8_t task_mode, uint8_t enable)
{
	static uint8_t index = 0;
	if (index < hSCH_MAX_TASKS)
	{
		hSCH_tasks_G[index].pTask  = pFunction;
		hSCH_tasks_G[index].Delay  = delay;
		hSCH_tasks_G[index].Period = period;
		hSCH_tasks_G[index].RunMe  = 0;
		hSCH_tasks_G[index].Co_op  = task_mode;
		hSCH_tasks_G[index].Enable = enable;
		index++;
	}
	return &hSCH_tasks_G[index-1];
}

/*******************************************************************************
 * Function: SCH_Delete_Task
 ******************************************************************************/
void SCH_Delete_Task(sTaskH *task) 
{
	task->pTask = NULL;
	task->Delay   = 0;
	task->Period  = 0;
	task->RunMe   = 0;
	task->Enable  = 0;
}

/*******************************************************************************
 * Function: SCH_Enable_Task
 ******************************************************************************/
void SCH_Enable_Task(sTaskH *task) 
{
	task->Enable  = 1;
	task->Delay = task->Period;
}

/*******************************************************************************
 * Function: SCH_Disable_Task
 ******************************************************************************/
void SCH_Disable_Task(sTaskH *task) 
{
	task->Enable  = 0;
}

/*******************************************************************************
 * Function: SCH_Update
 ******************************************************************************/
void SCH_Update(void) 
{
	uint8_t index;

	for (index = 0; index < hSCH_MAX_TASKS; index++)
	{
		if(hSCH_tasks_G[index].Enable)
		{
			if(hSCH_tasks_G[index].Delay == 0)
			{
				if(hSCH_tasks_G[index].Co_op)
				{
					hSCH_tasks_G[index].RunMe += 1;
				}
				else
				{
					if(hSCH_tasks_G[index].pTask != NULL)
					{
						hSCH_tasks_G[index].pTask();
						hSCH_tasks_G[index].RunMe -= 1;
					}
				}

				if(hSCH_tasks_G[index].Period)
				{
					hSCH_tasks_G[index].Delay = hSCH_tasks_G[index].Period;
				}
			}
			else
			{
				hSCH_tasks_G[index].Delay -= 1;
			}
		}
	}
}

/**
 * @brief  SYSTICK callback.
 * @retval None
 */
void HAL_SYSTICK_Callback(void)
{
	SCH_Update();
}
