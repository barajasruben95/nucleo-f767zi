/**
 ******************************************************************************
 * @file    Task_Scheduler.h
 * @author  Ruben Barajas
 * @version V1.0
 * @date    21-February-2024
 * @brief   HEADER Cooperative scheduler RTOS
 ******************************************************************************
 */

#ifndef __TASK_SCHEDULER_H
#define __TASK_SCHEDULER_H

#include "stm32f7xx.h"

/** MAXIMUM NUMBER OF TASK ALLOWED */
#define hSCH_MAX_TASKS  15

/** DELAY TASK OPTIONS */
#define hSCH_TASK_NODELAY   0    /** Do not delay task */

/* ENABLE TASK OPTIONS */
#define hSCH_TASK_ENABLE    1    /** Task is enable and will run in the CPU */
#define hSCH_TASK_DISABLE   0    /** Task is disabled and will not run in the CPU */

/* TASK MODE OPTIONS */
#define hSCH_TASK_NONCOOPERATIVE   1    /** Task can not be preempted for another higher priority task */
#define hSCH_TASK_COOPERATIVE      0    /** Task can be preempted for another higher priority task */

typedef void *TASK_SCH(void);
typedef struct 
{
    uint8_t  Enable;
    uint8_t  RunMe;
    uint8_t  Co_op;
    uint32_t Delay;
    uint32_t Period;
    TASK_SCH *pTask;
}sTaskH; 

/*******************************************************************************
 * Function: hSCH_Init
 ******************************************************************************/
void hSCH_Init(void);

/*******************************************************************************
 * Function: hSCH_Dispatch_Tasks
 ******************************************************************************/
void hSCH_Dispatch_Tasks(void);

/*******************************************************************************
 * Function: SCH_Add_Task
 ******************************************************************************/
sTaskH *SCH_Add_Task(TASK_SCH *pFunction, uint32_t delay, uint32_t period, uint8_t task_mode, uint8_t enable);

/*******************************************************************************
 * Function: SCH_Delete_Task
 ******************************************************************************/
void SCH_Delete_Task(sTaskH *task);

/*******************************************************************************
 * Function: SCH_Enable_Task
 ******************************************************************************/
void SCH_Enable_Task(sTaskH *task);

/*******************************************************************************
 * Function: SCH_Disable_Task
 ******************************************************************************/
void SCH_Disable_Task(sTaskH *task);

/*******************************************************************************
 * Function: SCH_Update
 ******************************************************************************/
void SCH_Update(void);

#endif
